package com.getjavajob.training.algo08.kashapovv.Utils;

import java.util.Arrays;

/**
 * Created by Вадим on 21.11.2016.
 */
public class Assert {
    public static void assertEquals(String testName, int excepted, int actual) {
        if (excepted == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.printf("%s not passed, excepted: %d, actual: %d%n", testName, excepted, actual);
        }
    }

    public static void assertEquals(String testName, boolean excepted, boolean actual) {
        if (excepted == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.printf("%s not passed, excepted: %b, actual: %b%n", testName, excepted, actual);
        }
    }

    public static void assertEquals(String testName, String excepted, String actual) {
        if (excepted.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.printf("%s not passed, excepted: %s, actual: %s%n", testName, excepted, actual);
        }
    }

    public static void assertEquals(String testName, int[] excepted, int[] actual) {
        if (Arrays.equals(excepted, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.printf("%s not passed, excepted: %s, actual: %s%n", testName, Arrays.toString(excepted)
                    , Arrays.toString(actual));
        }
    }

    public static void fail(String msg) {
        throw new AssertionError(msg);
    }
}
