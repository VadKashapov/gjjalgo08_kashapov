package com.getjavajob.training.algo08.kashapovv.Utils;

import java.util.Scanner;

/**
 * Created by Вадим on 21.11.2016.
 */
public class ConsoleIO {
    public static int readFromConsoleInt() {
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    public static double readFromConsoleDouble() {
        Scanner in = new Scanner(System.in);
        return in.nextDouble();
    }

    public static String readFromConsoleString() {
        Scanner in = new Scanner(System.in);
        return in.next();
    }

    public static byte readFromConsoleByte() {
        Scanner in = new Scanner(System.in);
        return in.nextByte();
    }
}
