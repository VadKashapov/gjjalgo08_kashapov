package com.getjavajob.training.algo08.kashapovv.Utils;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Вадим on 26.11.2016.
 */
public class StopWatch {
    private static long start;

    public static long start() {
        start = currentTimeMillis();
        return start;
    }

    public static long getElapsedTime() {
        return currentTimeMillis() - start;
    }
}
