package com.getjavajob.training.algo08.kashapovv.lesson01;

import static com.getjavajob.training.algo08.kashapovv.Utils.ConsoleIO.readFromConsoleByte;
import static com.getjavajob.training.algo08.kashapovv.Utils.ConsoleIO.readFromConsoleInt;

/**
 * Created by Вадим on 21.11.2016.
 */
public class Task06 {
    public static void main(String[] args) {
        int power = readFromConsoleInt();
        int result = getPowOfTwoLowest5bits(power);
        System.out.println(result);

        int powerN = readFromConsoleInt();
        int powerM = readFromConsoleInt();
        result = getPowOfTwoLowest5bits(powerN, powerM);
        System.out.println(result);

        int number = readFromConsoleInt();
        int bitPos = readFromConsoleInt();
        int resultReset = resetLowerBits(number, bitPos);
        System.out.println(resultReset);

        number = readFromConsoleInt();
        bitPos = readFromConsoleInt();
        int resultSet = set1NthBit(number, bitPos);
        System.out.println(resultSet);

        number = readFromConsoleInt();
        bitPos = readFromConsoleInt();
        int resultInvert = invertNthBit(number, bitPos);
        System.out.println(resultInvert);

        number = readFromConsoleInt();
        bitPos = readFromConsoleInt();
        int resultZero = zeroNthBit(number, bitPos);
        System.out.println(resultZero);

        number = readFromConsoleInt();
        bitPos = readFromConsoleInt();
        int resultReturn = returnLowerBits(number, bitPos);
        System.out.println(resultReturn);

        number = readFromConsoleInt();
        bitPos = readFromConsoleInt();
        resultReturn = returnNthBit(number, bitPos);
        System.out.println(resultReturn);

        byte byteNum = readFromConsoleByte();
        String bin = byteToBin(byteNum);
        System.out.println(bin);
    }

    /**
     * Method get power of two by lowest 5 bit number;
     *
     * @param power number lower than 5 bit size;
     * @return result number of getting power;
     */
    public static int getPowOfTwoLowest5bits(int power) {
        if (power == 0) {
            return 1;
        }
        if (power < 0 || power >= 31) {
            return -1;
        }
        int multiplicant = 2;
        for (int i = 1; i < power; i++) {
            multiplicant <<= 1;
        }
        return multiplicant;
    }

    /**
     * Method sum two getting powers of two by lowest 5 bit N,M numbers
     *
     * @param powerN power number N;
     * @param powerM power number M;
     * @return return result of sum of two power getting of two;
     */
    public static int getPowOfTwoLowest5bits(int powerN, int powerM) {
        int resultN = getPowOfTwoLowest5bits(powerN);
        int resultM = getPowOfTwoLowest5bits(powerM);
        if (resultM == -1 || resultN == -1) {
            return -1;
        }
        return (resultM & resultN) << 1 | resultM ^ resultN; // x + y = 2(x & y) | x ^ y
    }

    /**
     * Method reset bits lower bitLowerPos position by mask;
     *
     * @param number
     * @param bitLowerPos limit position on reset lower bits
     * @return number with reset bits;
     */
    public static int resetLowerBits(int number, int bitLowerPos) {
        if (bitLowerPos <= 0) {
            return -1;
        }
        int mask = ~1;
        mask <<= bitLowerPos;
        mask >>= 1;
        return number & mask;
    }

    /**
     * Method set 1 to Nth bit of number;
     *
     * @param number
     * @param bitPos position to set;
     * @return number with Nth set position;
     */
    public static int set1NthBit(int number, int bitPos) {
        if (bitPos <= 0) {
            return -1;
        }
        int mask = 1;
        mask <<= bitPos;
        mask >>= 1;
        return number | mask;
    }

    /**
     * Method invert Nth bit of number;
     *
     * @param number
     * @param bitPos bit position to invert;
     * @return number with Nth inverted position;
     */
    public static int invertNthBit(int number, int bitPos) {
        if (bitPos <= 0) {
            return -1;
        }
        int mask = 1;
        mask <<= bitPos;
        mask >>= 1;
        return number ^ mask;
    }

    /**
     * Method zero Nth bit of number
     *
     * @param number
     * @param bitPos bit position to zero;
     * @return number with Nth zero position;
     */
    public static int zeroNthBit(int number, int bitPos) {
        if (bitPos <= 0) {
            return -1;
        }
        int mask = 1;
        mask <<= bitPos;
        mask >>= 1;
        mask = ~mask;
        return number & mask;
    }

    /**
     * Method return number only with N-lower bits
     *
     * @param number
     * @param bitLowerPos limit position to return lower bits;
     * @return number with N-lower bits;
     */
    public static int returnLowerBits(int number, int bitLowerPos) {
        if (bitLowerPos <= 0) {
            return -1;
        }
        int mask = ~1;
        mask <<= bitLowerPos;
        mask >>= 1;
        mask = ~mask;
        return number & mask;
    }

    /**
     * Method return Nth bit of number;
     *
     * @param number
     * @param bitPos position to return bit;
     * @return bit of number;
     */
    public static int returnNthBit(int number, int bitPos) {
        if (bitPos <= 0) {
            return -1;
        }
        int mask = 1;
        mask <<= bitPos;
        mask >>= 1;
        return (number & mask) << 1 >> bitPos;
    }

    /**
     * Method return bin representation of byte
     *
     * @param number
     * @return bin representation of byte;
     */
    public static String byteToBin(byte number) {
        String bin = "";
        int bit = 0;
        int bitPos = getPowOfTwoLowest5bits(7);
        while ((bitPos & 1) == 0) {
            int counter = bitPos;
            bit = (number & bitPos);
            while ((counter & 1) == 0) {
                bit >>= 1;
                counter >>= 1;
            }
            bin += bit;
            bitPos >>= 1;
        }
        bin += (number & bitPos);
        return bin;
    }
}