package com.getjavajob.training.algo08.kashapovv.lesson01;

import static com.getjavajob.training.algo08.kashapovv.Utils.Assert.assertEquals;
import static com.getjavajob.training.algo08.kashapovv.lesson01.Task06.*;

/**
 * Created by Вадим on 21.11.2016.
 */
public class Task06Test {
    public static void main(String[] args) {
        testGetPowOfTwo5bitA();
        testGetPowOfTwo5bitB();
        testResetLowerBits();
        testSet1NthBit();
        testInvertNthBit();
        testZeroNthBit();
        testReturnLowerBits();
        testReturnNthBit();
        testByteToBin();
    }

    public static void testGetPowOfTwo5bitA() {
        assertEquals("Task06.testGetPowOfTwo5bitA.4", 0b10000, getPowOfTwoLowest5bits(0b100));
        assertEquals("Task06.testGetPowOfTwo5bitA.0", 0b1, getPowOfTwoLowest5bits(0b0));
        assertEquals("Task06.testGetPowOfTwo5bitA.1", 0b10, getPowOfTwoLowest5bits(0b1));
        assertEquals("Task06.testGetPowOfTwo5bitA.5", 0b100000, getPowOfTwoLowest5bits(0b101));
        assertEquals("Task06.testGetPowOfTwo5bitA.Error", -1, getPowOfTwoLowest5bits(31));
    }

    public static void testGetPowOfTwo5bitB() {
        assertEquals("Task06.testGetPowOfTwo5bitB.4", 0b10100, getPowOfTwoLowest5bits(0b100, 0b10)); // 2^4+2^2 = 16+4 = 20
        assertEquals("Task06.testGetPowOfTwo5bitB.5,3", 0b101000, getPowOfTwoLowest5bits(0b11, 0b101)); // 2^5+2^3 = 32 + 8 = 40
        assertEquals("Task06.testGetPowOfTwo5bitB.Error", -1, getPowOfTwoLowest5bits(31, 2));
    }

    public static void testResetLowerBits() {
        assertEquals("Task06.testResetLowerBits.4bitsFor31", 0b10000, resetLowerBits(0b11111, 4));
        assertEquals("Task06.testResetLowerBits.4bitsFor1001010", 0b1000000, resetLowerBits(0b1001010, 4));
        assertEquals("Task06.testResetLowerBits.Error", -1, resetLowerBits(22, 0));
    }

    public static void testSet1NthBit() {
        assertEquals("Task06.testSet1NthBit.4thFor0", 0b1000, set1NthBit(0b0, 4));
        assertEquals("Task06.testSet1NthBit.4thFor10100101", 0b11100101, set1NthBit(0b10100101, 7));
        assertEquals("Task06.testSet1NthBit.Error", -1, set1NthBit(22, 0));
    }

    public static void testInvertNthBit() {
        assertEquals("Task06.testInvertNthBit.4thFor1000", 0b0, invertNthBit(0b1000, 4));
        assertEquals("Task06.testInvertNthBit.4thFor10100001", 0b10000001, invertNthBit(0b10100001, 6));
        assertEquals("Task06.testInvertNthBit.Error", -1, invertNthBit(22, 0));
    }

    public static void testZeroNthBit() {
        assertEquals("Task06.testZeroNthBit.2thFor1010", 0b1000, zeroNthBit(0b1010, 2));
        assertEquals("Task06.testZeroNthBit.4thFor1011", 0b11, zeroNthBit(0b1011, 4));
        assertEquals("Task06.testZeroNthBit.Error", -1, zeroNthBit(22, 0));
    }

    public static void testReturnLowerBits() {
        assertEquals("Task06.testReturnLowerBits.3bitsFor1011", 0b11, returnLowerBits(0b1011, 3));
        assertEquals("Task06.testReturnLowerBits.4bitsFor101001", 0b1001, returnLowerBits(0b101001, 4));
        assertEquals("Task06.testReturnLowerBits.Error", -1, returnLowerBits(22, 0));
    }

    public static void testReturnNthBit() {
        assertEquals("Task06.testReturnNthBit.4thFor1011", 0b1, returnNthBit(0b1011, 4));
        assertEquals("Task06.testReturnNthBit.3thFor1011", 0b0, returnNthBit(0b1011, 3));
        assertEquals("Task06.testReturnNthBit.Error", -1, returnNthBit(22, 0));
    }

    public static void testByteToBin() {
        assertEquals("Task06.testByteToBin.4bitsFor10001", "00010001", byteToBin((byte) 0b10001));
        assertEquals("Task06.testByteToBin.4bitsFor10101", "00010101", byteToBin((byte) 0b10101));
    }
}
