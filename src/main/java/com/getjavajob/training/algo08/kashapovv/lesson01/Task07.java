package com.getjavajob.training.algo08.kashapovv.lesson01;

import java.util.Arrays;

import static com.getjavajob.training.algo08.kashapovv.Utils.ConsoleIO.readFromConsoleInt;

/**
 * Created by Вадим on 22.11.2016.
 */
public class Task07 {
    public static void main(String[] args) {
        int value1 = readFromConsoleInt();
        int value2 = readFromConsoleInt();

        int[] result = swapSum(value1, value2);
        System.out.println(Arrays.toString(result));

        result = swapMul(value1, value2);
        System.out.println(Arrays.toString(result));

        result = swapXor(value1, value2);
        System.out.println(Arrays.toString(result));

        result = swapBitwiseSum(value1, value2);
        System.out.println(Arrays.toString(result));
    }

    public static int[] swapSum(int value1, int value2) {
        value1 += value2;
        value2 = value1 - value2;
        value1 -= value2;
        return new int[]{value1, value2};
    }

    public static int[] swapMul(int value1, int value2) {
        value1 *= value2;
        value2 = value1 / value2;
        value1 /= value2;
        return new int[]{value1, value2};
    }

    public static int[] swapXor(int value1, int value2) {
        value1 ^= value2;
        value2 = value1 ^ value2;
        value1 ^= value2;
        return new int[]{value1, value2};
    }

    public static int[] swapBitwiseSum(int value1, int value2) {
        value1 |= value2;
        value2 = value1 | (~value2 | 1);
        value1 |= (~value2 | 1);
        return new int[]{value1, value2};
    }
}
