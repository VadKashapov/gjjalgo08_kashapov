package com.getjavajob.training.algo08.kashapovv.lesson01;

import static com.getjavajob.training.algo08.kashapovv.Utils.Assert.assertEquals;
import static com.getjavajob.training.algo08.kashapovv.lesson01.Task07.swapSum;

/**
 * Created by Вадим on 22.11.2016.
 */
public class Task07Test {
    public static void main(String[] args) {
        testSwapSum();
        testSwapMul();
        testSwapXor();
        testSwapBitwiseSum();
    }

    public static void testSwapSum() {
        assertEquals("Task07.testSwapSum.10,7", new int[]{7, 10}, swapSum(10, 7));
    }

    public static void testSwapMul() {
        assertEquals("Task07.testSwapMul.10,7", new int[]{7, 10}, swapSum(10, 7));
    }

    public static void testSwapXor() {
        assertEquals("Task07.testSwapXor.10,7", new int[]{7, 10}, swapSum(10, 7));
    }

    public static void testSwapBitwiseSum() {
        assertEquals("Task07.testSwapBitwiseSum.10,7", new int[]{7, 10}, swapSum(10, 7));
    }
}
