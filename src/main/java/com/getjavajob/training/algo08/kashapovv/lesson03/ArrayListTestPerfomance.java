package com.getjavajob.training.algo08.kashapovv.lesson03;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.start;

/**
 * Created by Вадим on 02.12.2016.
 */
public class ArrayListTestPerfomance {
    public static void main(String[] args) {
        testPerformanceAddBegin();
        testPerformanceRemoveBegin();
        testPerformanceAddMiddle();
        testPerformanceRemoveMiddle();
        testPerformanceAddEnd();
        testPerformanceRemoveEnd();
        testRemoveObject();
    }

    public static void fillArray(ArrayList arrayList, int size) {
        for (int i = 0; i < size; i++) {
            arrayList.add(i);
        }
    }

    public static void testPerformanceAddBegin() {
        ArrayList<Object> arrayList = new ArrayList<>();

        start();
        for (int i = 0; i < 120_000; i++) {
            arrayList.add(0, i);
        }
        System.out.println("arrayList.add(int, e): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceRemoveBegin() {
        ArrayList<Object> arrayList = new ArrayList<>();
        fillArray(arrayList, 120_000);
        start();
        for (int i = 0; i < 120_000; i++) {
            arrayList.remove(0);
        }
        System.out.println("arrayList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddMiddle() {
        ArrayList<Object> arrayList = new ArrayList<>();
        fillArray(arrayList, 100_000);

        start();
        for (int i = 0; i < 100_000; i++) {
            arrayList.add(50_000, i);
        }
        System.out.println("arrayList.add(int, e): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceRemoveMiddle() {
        ArrayList<Object> arrayList = new ArrayList<>();
        fillArray(arrayList, 300_000);
        start();
        for (int i = 0; i < 150_000; i++) {
            arrayList.remove(150_000);
        }
        System.out.println("arrayList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddEnd() {
        ArrayList<Object> arrayList = new ArrayList<>();
        fillArray(arrayList, 1_000_000);

        start();
        for (int i = 0; i < 10_000_000; i++) {
            arrayList.add(i);
        }
        System.out.println("arrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceRemoveEnd() {
        ArrayList<Object> arrayList = new ArrayList<>();
        fillArray(arrayList, 10_000_000);

        start();
        for (int i = 0; i < 10_000_000; i++) {
            arrayList.remove(arrayList.size() - 1);
        }
        System.out.println("arrayList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testRemoveObject() {
        ArrayList<Object> arrayList = new ArrayList<>();
        fillArray(arrayList, 40_000);

        start();
        for (int i = 0; i < 30_000; i++) {
            arrayList.remove((Object) (30_000 - i));
        }
        System.out.println("arrayList.remove(e): " + getElapsedTime() + " ms");
    }
}
