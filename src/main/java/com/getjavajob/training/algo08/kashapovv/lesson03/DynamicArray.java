package com.getjavajob.training.algo08.kashapovv.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;

import static java.lang.System.arraycopy;

/**
 * Created by Вадим on 24.11.2016.
 */

public class DynamicArray {
    private static final int DEFAULT_SIZE = 10;
    private static final int START_INDEX = 0;
    private static final double EXPAND_SIZE_ON = 1.5;

    private int modCount;
    private int internalSize;
    private Object[] elements;

    public DynamicArray() {
        this(DEFAULT_SIZE);
    }

    public DynamicArray(int size) {
        elements = new Object[size];
        internalSize = START_INDEX;
    }

    private void checkIndicesRangeAdd(int index) {
        if (index < 0 || index > internalSize) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    private void checkIndicesRange(int index) {
        if (index < 0 || index >= internalSize) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

    }

    private boolean growIfFull() {
        if (internalSize >= elements.length) {
            Object[] elementsNew = new Object[(int) (elements.length * EXPAND_SIZE_ON)];
            arraycopy(elements, 0, elementsNew, 0, internalSize);
            elements = elementsNew;
            return true;
        } else {
            return false;
        }
    }

    public boolean add(Object e) throws ArrayIndexOutOfBoundsException {
        growIfFull();
        elements[internalSize++] = e;
        modCount++;
        return true;
    }

    public void add(int i, Object e) throws ArrayIndexOutOfBoundsException {
        checkIndicesRangeAdd(i);
        growIfFull();
        arraycopy(elements, i, elements, i + 1, internalSize - i);
        elements[i] = e;
        internalSize++;
        modCount++;
    }

    public Object set(int i, Object e) throws ArrayIndexOutOfBoundsException {
        checkIndicesRange(i);
        Object oldElement = elements[i];
        elements[i] = e;
        return oldElement;
    }

    public Object get(int i) throws ArrayIndexOutOfBoundsException {
        checkIndicesRange(i);
        return elements[i];
    }

    public Object remove(int i) throws ArrayIndexOutOfBoundsException {
        checkIndicesRange(i);
        Object oldElement = elements[i];
        arraycopy(elements, i + 1, elements, i, internalSize - i - 1);
        internalSize--;
        modCount++;
        return oldElement;
    }

    public boolean remove(Object e) throws ArrayIndexOutOfBoundsException {
        int index = indexOf(e);
        if (index >= 0) {
            remove(index);
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return internalSize;
    }

    public int indexOf(Object e) {
        for (int i = 0; i < internalSize; i++) {
            if (elements[i].equals(e)) {
                return i;
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    public Object[] toArray() {
        return Arrays.copyOf(elements, internalSize);
    }

    public ListIterator listIterator() {
        return new ListIterator();
    }

    protected class ListIterator {
        private int index;
        private int expectedModCount;

        public ListIterator() {
            index = START_INDEX;
            expectedModCount = modCount;
        }

        private boolean checkModCount() {
            if (expectedModCount == modCount) {
                return true;
            } else {
                throw new ConcurrentModificationException("Array struct has changed");
            }
        }

        public boolean hasNext() {
            return internalSize > index + 1;
        }

        public Object next() throws ConcurrentModificationException, ArrayIndexOutOfBoundsException {
            checkModCount();
            checkIndicesRange(index + 1);
            index++;
            return elements[index];
        }

        public boolean hasPrevious() {
            return index - 1 > 0;
        }

        public Object previous() throws ConcurrentModificationException, ArrayIndexOutOfBoundsException {
            checkModCount();
            checkIndicesRange(index - 1);
            index--;
            return elements[index];
        }

        public int nextIndex() {
            return index + 1;
        }

        public int previousIndex() {
            return index - 1;
        }

        public void remove() throws ConcurrentModificationException {
            checkModCount();
            DynamicArray.this.remove(index);
            expectedModCount++;
        }

        public void set(Object e) {
            DynamicArray.this.set(index, e);
        }

        public void add(Object e) throws ConcurrentModificationException {
            checkModCount();
            DynamicArray.this.add(index, e);
            expectedModCount++;
        }
    }
}