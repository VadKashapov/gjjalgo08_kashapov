package com.getjavajob.training.algo08.kashapovv.lesson03;

import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.start;

/**
 * Created by Вадим on 26.11.2016.
 */
public class DynamicArrayPerfomanceTest {
    public static void main(String[] args) {
        testPerformanceAddBegin();
        testPerformanceRemoveBegin();
        testPerformanceAddMiddle();
        testPerformanceRemoveMiddle();
        testPerformanceAddEnd();
        testPerformanceRemoveEnd();
        testRemoveObject();
    }

    public static void fillArray(DynamicArray dynamicArray, int size) {
        for (int i = 0; i < size; i++) {
            dynamicArray.add(i);
        }
    }

    public static void testPerformanceAddBegin() {
        DynamicArray dynamicArray = new DynamicArray();

        start();
        for (int i = 0; i < 120_000; i++) {
            dynamicArray.add(0, i);
        }
        System.out.println("Begin:");
        System.out.println("DynamicArray.add(int, e): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceRemoveBegin() {
        DynamicArray dynamicArray = new DynamicArray();
        fillArray(dynamicArray, 120_000);

        start();
        for (int i = 0; i < 120_000; i++) {
            dynamicArray.remove(0);
        }
        System.out.println("DynamicArray.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddMiddle() {
        DynamicArray dynamicArray = new DynamicArray();
        fillArray(dynamicArray, 100_000);

        start();
        for (int i = 0; i < 100_000; i++) {
            dynamicArray.add(50_000, i);
        }
        System.out.println("Middle:");
        System.out.println("DynamicArray.add(int, e): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceRemoveMiddle() {
        DynamicArray dynamicArray = new DynamicArray();
        fillArray(dynamicArray, 300_000);

        start();
        for (int i = 0; i < 150_000; i++) {
            dynamicArray.remove(150_000);
        }
        System.out.println("DynamicArray.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddEnd() {
        DynamicArray dynamicArray = new DynamicArray();
        fillArray(dynamicArray, 1_000_000);

        start();
        for (int i = 0; i < 10_000_000; i++) {
            dynamicArray.add(i);
        }
        System.out.println("End:");
        System.out.println("DynamicArray.add(e): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceRemoveEnd() {
        DynamicArray dynamicArray = new DynamicArray();
        fillArray(dynamicArray, 10_000_000);

        start();
        for (int i = 0; i < 10_000_000; i++) {
            dynamicArray.remove(dynamicArray.size() - 1);
        }
        System.out.println("DynamicArray.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testRemoveObject() {
        DynamicArray dynamicArray = new DynamicArray();
        fillArray(dynamicArray, 40_000);

        start();
        for (int i = 0; i < 30_000; i++) {
            dynamicArray.remove((Object) (30_000 - i));
        }
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
    }
}


