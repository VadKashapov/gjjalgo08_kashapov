package com.getjavajob.training.algo08.kashapovv.lesson04;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 29.11.2016.
 */
public class CollectionTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    private Collection<Integer> list;

    @Before
    public void setUp() {
        list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
    }

    @After
    public void tearDown() {
        list.clear();
    }

    @Test
    public void testAdd() {
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 5}, list.toArray());
        list.add(null);
    }

    @Test
    public void testAddAll() {
        list.addAll(asList(1, 2, 3));
        assertEquals("testAddAll", new Integer[]{1, 2, 3, 4, 5, 1, 2, 3}, list.toArray());
        list.addAll(asList(null, 1));
    }

    @Test
    public void testIsEmpty() {
        assertEquals("testAddAll", false, list.isEmpty());
        list.clear();
        assertEquals("testAddAll", true, list.isEmpty());
    }

    @Test
    public void testContains() {
        assertEquals("testAddAll", false, list.contains(6));
        assertEquals("testAddAll", true, list.contains(5));
        list.contains(null);
    }

    @Test()
    public void testContainsAll() {
        assertEquals("testContainsAll", true, list.containsAll(asList(5, 3, 2, 1, 4)));

        list.containsAll(asList(1, null));
    }

    @Test
    public void testRetainAll() {
        List<Integer> elementsRetain = asList(5, 2);
        list.retainAll(elementsRetain);
        assertEquals("testRetainAll", new Integer[]{2, 5}, list.toArray());
        list.retainAll(asList(1, null));
    }

    @Test
    public void testRemove() {
        list.remove(4);
        list.remove(2);
        assertEquals("testRemove", new Integer[]{1, 3, 5}, list.toArray());
        list.remove(null);
    }

    @Test
    public void testRemoveAll() {
        List<Integer> elementsRemove = asList(5, 2);
        list.removeAll(elementsRemove);
        assertEquals("testRemoveAll", new Integer[]{1, 3, 4}, list.toArray());
        list.removeAll(asList(1, null));
    }

    @Test
    public void testClear() {
        list.clear();
        assertEquals("testClear", new Integer[]{}, list.toArray());
    }

    @Test
    public void testSize() {
        assertEquals("testSize", 5, list.size());
    }

    @Test
    public void testToArray1() {
        assertEquals("testToArray", new Integer[]{1, 2, 3, 4, 5}, list.toArray());
    }

    @Test
    public void testToArray2() {
        assertEquals("testToArray(Object[] a)", new Integer[]{1, 2, 3, 4, 5, null, null},
                list.toArray(new Object[7]));
    }

    @Test
    public void testEquals() {
        assertEquals("testEquals", true, list.equals(asList(1, 2, 3, 4, 5)));
        assertEquals("testEquals", false, list.equals(asList(1, 2, 4, 5)));
    }

    @Test
    public void testIterator() {
        Iterator<Integer> iter = list.iterator();
        assertEquals("testIterator.hasNext1", true, iter.hasNext());
        assertEquals("testIterator.Next", 1, (int) iter.next());
        assertEquals("testIterator.Next", 2, (int) iter.next());
        iter.remove();
        assertEquals("testIterator.remove", new Integer[]{1, 3, 4, 5}, list.toArray());
        list.clear();
        assertEquals("testIterator.hasNext2", true, iter.hasNext());

        thrown.expect(ConcurrentModificationException.class);
        iter.next();
    }
}

