package com.getjavajob.training.algo08.kashapovv.lesson04;

import java.util.ConcurrentModificationException;

/**
 * Created by Вадим on 27.11.2016.
 */
public class DoublyLinkedList<E> extends AbstractList<E> {
    private static final int START_SIZE = 0;
    private static final int START_INDEX = 0;

    private int modCount;
    private int size;
    private Element<E> first;
    private Element<E> last;

    public DoublyLinkedList() {
        size = START_SIZE;
        first = new Element<>(last, last, null);
        last = first;
    }

    private boolean checkIndexRange(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size);
        }
        return true;
    }

    private Element<E> findElement(int index) throws IndexOutOfBoundsException {
        checkIndexRange(index);
        Element<E> element;
        if (index > size / 2) {
            element = last;
            for (int i = size - 1; i > index; i--) {
                element = element.prev;
            }
        } else {
            element = first.next;
            for (int i = 0; i < index; i++) {
                element = element.next;
            }
        }
        return element;
    }

    private Element<E> findElement(Object value) {
        Element<E> element = first;
        for (int i = 0; i < size; i++) {
            if (element.value != value) {
                element = element.next;
            } else {
                break;
            }
        }
        return element.value == value ? element : null;
    }

    @Override
    public boolean add(E value) {
        Element<E> element = new Element<>(last, null, value);
        last.next = element;
        last = element;
        size++;
        modCount++;
        return true;
    }

    @Override
    public void add(int index, E value) throws IndexOutOfBoundsException {
        if (size == 0) {
            add(value);
        } else {
            Element<E> element = findElement(index);
            Element<E> newElement = new Element<>(element.prev, element, value);

            element.prev.next = newElement;
            element.prev = newElement;
            size++;
            modCount++;
        }
    }

    @Override
    public boolean remove(Object value) {
        Element<E> element = findElement(value);
        if (element != null) {
            element.prev.next = element.next;
            element.next.prev = element.prev;
            size--;
            modCount++;
            return true;
        }
        return false;
    }

    @Override
    public E remove(int index) throws IndexOutOfBoundsException {
        Element<E> element = findElement(index);
        final Element<E> next = element.next;
        final Element<E> prev = element.prev;

        if (next == null) {
            element.value = null;
            prev.next = null;
            last = prev;
        } else {
            prev.next = next;
            next.prev = prev;
        }

        size--;
        modCount++;
        return element.value;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     * @throws ClassCastException   if the type of the specified element
     *                              is incompatible with this list
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this
     *                              list does not permit null elements
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     */
    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public E set(int index, E value) throws IndexOutOfBoundsException {
        checkIndexRange(index);
        Element<E> element = findElement(index);
        E oldValue = element.value;
        element.value = value;
        return oldValue;
    }

    @Override
    public E get(int index) throws IndexOutOfBoundsException {
        return findElement(index).value;
    }

    @Override
    public int size() {
        return size;
    }

    /**
     * Returns <tt>true</tt> if this list contains the specified element.
     * More formally, returns <tt>true</tt> if and only if this list contains
     * at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param o element whose presence in this list is to be tested
     * @return <tt>true</tt> if this list contains the specified element
     * @throws ClassCastException   if the type of the specified element
     *                              is incompatible with this list
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this
     *                              list does not permit null elements
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     */
    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Object[] toArray() {
        if (size == 0) {
            return new Object[]{};
        }
        Object[] elements = new Object[size];
        Element<E> element = first.next;
        for (int i = 0; i < size; i++) {
            elements[i] = element.value;
            element = element.next;
        }
        return elements;
    }

    @Override
    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    protected static class Element<E> {
        E value;
        Element<E> prev;
        Element<E> next;

        public Element(Element<E> prev, Element<E> next, E value) {
            this.prev = prev;
            this.next = next;
            this.value = value;
        }
    }

    private class ListIteratorImpl implements java.util.ListIterator<E> {
        private int index;
        private Element<E> element;
        private int expectedModCount;

        public ListIteratorImpl(int index) {
            this.index = index;
            element = first;
            expectedModCount = modCount;
        }

        public ListIteratorImpl() {
            this(START_INDEX);
        }

        private boolean checkCommodation() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException("List has changed");
            }
            return true;
        }

        @Override
        public boolean hasNext() {
            return element.next != null;
        }

        @Override
        public E next() throws IndexOutOfBoundsException {
            checkIndexRange(index + 1);
            checkCommodation();
            element = element.next;
            index++;
            return element.value;
        }

        @Override
        public boolean hasPrevious() {
            return element.prev != null;
        }

        @Override
        public E previous() throws IndexOutOfBoundsException {
            checkIndexRange(index - 1);
            checkCommodation();
            element = element.prev;
            index--;
            return element.value;
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            checkCommodation();
            DoublyLinkedList.this.remove(index);
            expectedModCount++;
        }

        @Override
        public void set(E value) {
            checkCommodation();
            DoublyLinkedList.this.set(index, value);
        }

        @Override
        public void add(E value) {
            checkCommodation();
            DoublyLinkedList.this.add(index, value);
            expectedModCount++;
        }
    }
}