package com.getjavajob.training.algo08.kashapovv.lesson04;

import com.getjavajob.training.algo08.kashapovv.Utils.Assert;

import java.util.ListIterator;

import static com.getjavajob.training.algo08.kashapovv.Utils.Assert.assertEquals;

/**
 * Created by Вадим on 28.11.2016.
 */
public class DoublyLinkedListTest {
    public static void main(String[] args) {
        testAddRemoveBegin();
        testAddRemoveMiddle();
        testAddRemoveEnd();
        testListIterator();
        testListIteratorConcurrentException();
    }

    public static DoublyLinkedList fillDoublyList(int size) {
        DoublyLinkedList<Integer> elements = new DoublyLinkedList<>();
        for (int i = 0; i < size; i++) {
            elements.add(i);
        }
        return elements;
    }

    public static void testAddRemoveBegin() {
        DoublyLinkedList<Integer> elements = new DoublyLinkedList<>();
        elements.add(0, 0);
        elements.add(0, 1);
        elements.add(0, 2);
        elements.add(0, null);
        assertEquals("DoublyLinkedListTest.testAddRemoveBegin.Add", new Integer[]{null, 2, 1, 0},
                elements.toArray());

        elements.remove(0);
        elements.remove(0);
        elements.remove(0);
        assertEquals("DoublyLinkedListTest.testAddRemoveBegin.Add", new Integer[]{0},
                elements.toArray());
    }

    public static void testAddRemoveMiddle() {
        DoublyLinkedList<Integer> elements = fillDoublyList(5);
        elements.add(2, 5);
        elements.add(2, 6);
        elements.add(2, null);
        assertEquals("DoublyLinkedListTest.testAddRemoveMiddle.Add", new Integer[]{0, 1, null, 6, 5, 2, 3, 4},
                elements.toArray());

        elements.remove(2);
        elements.remove(2);
        elements.remove(2);
        assertEquals("DoublyLinkedListTest.testAddRemoveMiddle.Add", new Integer[]{0, 1, 2, 3, 4},
                elements.toArray());
    }

    public static void testAddRemoveEnd() {
        DoublyLinkedList<Integer> elements = fillDoublyList(5);
        elements.add(5);
        elements.add(6);
        elements.add(null);
        assertEquals("DoublyLinkedListTest.testAddRemoveEnd.Add", new Integer[]{0, 1, 2, 3, 4, 5, 6, null},
                elements.toArray());

        elements.remove(elements.size() - 1);
        elements.remove(elements.size() - 1);
        elements.remove(elements.size() - 1);
        assertEquals("DoublyLinkedListTest.testAddRemoveEnd.Add", new Integer[]{0, 1, 2, 3, 4},
                elements.toArray());
    }

    public static void testListIterator() {
        DoublyLinkedList<Integer> elements = fillDoublyList(4);
        ListIterator<Integer> listIterator = elements.listIterator();

        listIterator.hasNext();
        listIterator.hasPrevious();
        listIterator.remove();
        listIterator.add(10);
        listIterator.next();
        listIterator.set(20);
        assertEquals("Lesson03.DoublyLinkedList.testIterator", new Integer[]{10, 20, 2, 3}, elements.toArray());
    }

    public static void testListIteratorConcurrentException() {
        DoublyLinkedList<Integer> doublyLinkedList = fillDoublyList(7);
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();

        doublyLinkedList.add(7);
        try {
            listIterator.next();
            Assert.fail("Successful remove() completion"); // that will throw java.lang.AssertionError
        } catch (Exception e) {
            assertEquals("DoublyLinkedListTest.testListIteratorConcurrentException", "List has changed",
                    e.getMessage());
        }
    }
}