package com.getjavajob.training.algo08.kashapovv.lesson04;

import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.start;

/**
 * Created by Вадим on 28.11.2016.
 */
public class DoublyListPerfomanceTest {
    public static void main(String[] args) {
        testPerformanceAddRemoveBegin();
        testPerformanceAddRemoveMiddle();
        testPerformanceAddRemoveEnd();
    }

    public static void fillArray(DoublyLinkedList doublyLinkedList, int size) {
        for (int i = 0; i < size; i++) {
            doublyLinkedList.add(i);
        }
    }

    public static void testPerformanceAddRemoveBegin() {
        DoublyLinkedList doublyLinkedList = new DoublyLinkedList();

        start();
        for (int i = 0; i < 5_000_000; i++) {
            doublyLinkedList.add(0, i);
        }
        System.out.println("Begin:");
        System.out.println("DoublyLinkedList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 5_000_000; i++) {
            doublyLinkedList.remove(0);
        }
        System.out.println("DoublyLinkedList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddRemoveMiddle() {
        DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
        fillArray(doublyLinkedList, 40_000);

        start();
        for (int i = 0; i < 30_000; i++) {
            doublyLinkedList.add(15_000, i);
        }
        System.out.println("Middle:");
        System.out.println("DoublyLinkedList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 30_000; i++) {
            doublyLinkedList.remove(15_000);
        }
        System.out.println("DoublyLinkedList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddRemoveEnd() {
        DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
        fillArray(doublyLinkedList, 2_000_000);

        start();
        for (int i = 0; i < 2_000_000; i++) {
            doublyLinkedList.add(i);
        }
        System.out.println("End:");
        System.out.println("DoublyLinkedList.add(e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 2_000_000; i++) {
            doublyLinkedList.remove(doublyLinkedList.size() - 1);
        }
        System.out.println("DoublyLinkedList.remove(int): " + getElapsedTime() + " ms");
    }
}