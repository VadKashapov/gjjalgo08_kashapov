package com.getjavajob.training.algo08.kashapovv.lesson04;

import java.util.Arrays;
import java.util.ConcurrentModificationException;

import static java.lang.System.arraycopy;

/**
 * Created by Вадим on 27.11.2016.
 */
public class DynamicArray<E> extends AbstractList<E> {
    final private static int DEFAULT_SIZE = 10;
    final private static int START_INDEX = 0;
    final private static double EXPAND_SIZE_ON = 1.5;

    private int modCount;
    private int internalSize;
    private E[] elements;

    public DynamicArray() {
        this(DEFAULT_SIZE);
    }

    public DynamicArray(int size) {
        modCount = 0;
        elements = (E[]) new Object[size];
        internalSize = START_INDEX;
    }

    private void checkIndiceRangeAdd(int index) {
        if (index < 0 || index > internalSize || index + 1 > elements.length) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    private void checkIndiceRange(int index) {
        if (index < 0 || index >= internalSize || index + 1 > elements.length) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    private boolean growIfFull() {
        if (internalSize >= elements.length) {
            E[] elementsNew = (E[]) new Object[(int) (elements.length * EXPAND_SIZE_ON)];
            arraycopy(elements, 0, elementsNew, 0, internalSize);
            elements = elementsNew;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean add(E element) throws ArrayIndexOutOfBoundsException {
        growIfFull();
        elements[internalSize++] = element;
        modCount++;
        return true;
    }

    @Override
    public void add(int index, E element) throws ArrayIndexOutOfBoundsException {
        checkIndiceRangeAdd(index);
        growIfFull();
        arraycopy(elements, index, elements, index + 1, internalSize - index);
        elements[index] = element;
        internalSize++;
        modCount++;
    }

    @Override
    public E set(int index, E element) throws ArrayIndexOutOfBoundsException {
        checkIndiceRange(index);
        E oldElement = elements[index];
        elements[index] = element;
        return oldElement;
    }

    @Override
    public E get(int index) throws ArrayIndexOutOfBoundsException {
        checkIndiceRange(index);
        return elements[index];
    }

    @Override
    public E remove(int index) throws ArrayIndexOutOfBoundsException {
        checkIndiceRange(index);
        E oldElement = elements[index];
        arraycopy(elements, index + 1, elements, index, internalSize - index - 1);
        internalSize--;
        modCount++;
        return oldElement;
    }

    @Override
    public boolean remove(Object element) throws ArrayIndexOutOfBoundsException {
        int index = indexOf(element);
        if (index >= 0) {
            remove(index);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return internalSize;
    }

    @Override
    public int indexOf(Object element) {
        for (int i = 0; i < internalSize; i++) {
            if (elements[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(Object element) {
        return indexOf(element) >= 0;
    }

    @Override
    public E[] toArray() {
        return Arrays.copyOf(elements, internalSize);
    }

    @Override
    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl implements java.util.ListIterator<E> {
        private int index;
        private int expectedModCount;

        public ListIteratorImpl() {
            index = START_INDEX;
            expectedModCount = modCount;
        }

        private boolean checkModCount() {
            if (expectedModCount == modCount) {
                return true;
            } else {
                throw new ConcurrentModificationException("Array struct has changed");
            }
        }

        @Override
        public boolean hasNext() {
            return internalSize > index + 1;
        }

        @Override
        public E next() throws ConcurrentModificationException, ArrayIndexOutOfBoundsException {
            checkModCount();
            checkIndiceRange(index + 1);
            index++;
            return elements[index];
        }

        @Override
        public boolean hasPrevious() {
            return index - 1 > 0;
        }

        @Override
        public E previous() throws ConcurrentModificationException, ArrayIndexOutOfBoundsException {
            checkModCount();
            checkIndiceRange(index - 1);
            index--;
            return elements[index];
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() throws ConcurrentModificationException {
            checkModCount();
            DynamicArray.this.remove(index);
            expectedModCount++;
        }

        @Override
        public void set(E element) {
            DynamicArray.this.set(index, element);
        }

        @Override
        public void add(E element) throws ConcurrentModificationException {
            checkModCount();
            DynamicArray.this.add(index, element);
            expectedModCount++;
        }
    }
}