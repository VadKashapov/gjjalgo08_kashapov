package com.getjavajob.training.algo08.kashapovv.lesson04;

import com.getjavajob.training.algo08.kashapovv.Utils.Assert;

import java.util.ListIterator;

import static com.getjavajob.training.algo08.kashapovv.Utils.Assert.assertEquals;

/**
 * Created by Вадим on 05.12.2016.
 */
public class DynamicArrayTest {
    public static void main(String[] args) {
        testAddRemoveBegin();
        testAddRemoveMiddle();
        testAddRemoveEnd();
        testRemoveObject();
        testGetSet();
        testContain();
        testAddException();
        testRemoveException();
        testIterator();
        testIteratorConcurrentException();
    }

    public static DynamicArray fillDynArray(int size) {
        DynamicArray<Integer> elements = new DynamicArray<>(size);
        for (int i = 1; i <= size; i++) {
            elements.add(i);
        }
        return elements;
    }

    public static DynamicArray fillDynArray() {
        return fillDynArray(10);
    }

    public static void testAddRemoveBegin() {
        DynamicArray<Integer> elements = new DynamicArray<>(4);
        elements.add(0, null);
        elements.add(0, 2);
        elements.add(0, 3);
        elements.add(0, 4);
        assertEquals("Lesson03.DynamicArray.testAddRemoveBegin.Add", new Integer[]{4, 3, 2, null},
                elements.toArray());

        elements.remove(0);
        elements.remove(0);
        elements.remove(0);
        elements.remove(0);
        assertEquals("Lesson03.DynamicArray.testAddRemoveBegin.Remove", new Integer[]{},
                elements.toArray());
    }

    public static void testAddRemoveMiddle() {
        DynamicArray<Integer> elements = fillDynArray(7);

        elements.add(3, 10);
        elements.add(3, 11);
        elements.add(3, 12);
        elements.add(3, 13);
        assertEquals("Lesson03.DynamicArray.testAddRemoveMiddle.Add", new Integer[]{1, 2, 3, 13, 12, 11, 10, 4, 5, 6, 7},
                elements.toArray());

        elements.remove(3);
        elements.remove(3);
        elements.remove(3);
        assertEquals("Lesson03.DynamicArray.testAddRemoveMiddle.Remove", new Integer[]{1, 2, 3, 10, 4, 5, 6, 7},
                elements.toArray());
    }

    public static void testAddRemoveEnd() {
        DynamicArray<Integer> elements = fillDynArray(4);

        elements.add(10);
        elements.add(11);
        elements.add(12);
        elements.add(13);
        assertEquals("Lesson03.DynamicArray.testAddRemoveEnd.Add", new Integer[]{1, 2, 3, 4, 10, 11, 12, 13},
                elements.toArray());

        elements.remove(elements.size() - 1);
        elements.remove(elements.size() - 1);
        elements.remove(elements.size() - 1);
        assertEquals("Lesson03.DynamicArray.testAddRemoveEnd.Remove", new Integer[]{1, 2, 3, 4, 10},
                elements.toArray());
    }

    public static void testRemoveObject() {
        DynamicArray<Integer> elements = fillDynArray(7);
        elements.remove(1);
        elements.remove(5);
        elements.remove(3);
        elements.remove(7);
        assertEquals("Lesson03.DynamicArray.testRemoveObject", new Integer[]{2, 4, 6}, elements.toArray());
    }

    public static void testGetSet() {
        DynamicArray<Integer> elements = fillDynArray(4);

        assertEquals("Lesson03.DynamicArray.testGetObject", "3", elements.get(2).toString());
        elements.set(3, 6);
        assertEquals("Lesson03.DynamicArray.testSetObject", "6", elements.get(3).toString());
    }

    public static void testContain() {
        DynamicArray<Integer> elements = fillDynArray(5);
        assertEquals("Lesson03.DynamicArray.testContain.False", false, elements.contains(7));
        assertEquals("Lesson03.DynamicArray.testContain.True", true, elements.contains(3));
    }

    public static void testAddException() {
        DynamicArray<Integer> elements = fillDynArray();
        try {
            elements.add(12, 20);
            Assert.fail("Successful add() completion"); // that will throw java.lang.AssertionError
        } catch (Exception e) {
            assertEquals("Lesson03.DynamicArray.testAddException", "Array index out of range: 12", e.getMessage());
        }
    }

    public static void testRemoveException() {
        DynamicArray<Integer> elements = fillDynArray();
        try {
            elements.remove(12);
            Assert.fail("Successful remove() completion"); // that will throw java.lang.AssertionError
        } catch (Exception e) {
            assertEquals("Lesson03.DynamicArray.testRemoveException", "Array index out of range: 12",
                    e.getMessage());
        }
    }

    public static void testIterator() {
        DynamicArray<Integer> elements = fillDynArray(4);
        ListIterator<Integer> listIterator = elements.listIterator();

        listIterator.hasNext();
        listIterator.hasPrevious();
        listIterator.remove();
        listIterator.add(10);
        listIterator.next();
        listIterator.set(20);
        assertEquals("Lesson03.DynamicArray.testIterator", new Integer[]{10, 20, 3, 4}, elements.toArray());
    }

    public static void testIteratorConcurrentException() {
        DynamicArray<Integer> dynamicArray = fillDynArray(7);
        ListIterator<Integer> listIterator = dynamicArray.listIterator();

        dynamicArray.add(7);
        try {
            listIterator.next();
            Assert.fail("Successful remove() completion"); // that will throw java.lang.AssertionError
        } catch (Exception e) {
            assertEquals("Lesson03.DynamicArray.testRemoveException", "Array struct has changed",
                    e.getMessage());
        }
    }
}
