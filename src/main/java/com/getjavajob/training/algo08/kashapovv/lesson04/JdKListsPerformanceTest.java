package com.getjavajob.training.algo08.kashapovv.lesson04;

import java.util.ArrayList;
import java.util.LinkedList;

import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.start;

/**
 * Created by Вадим on 28.11.2016.
 */
public class JdKListsPerformanceTest {
    public static void main(String[] args) {
        testPerformanceAddRemoveBegin();
        testPerformanceAddRemoveMiddle();
        testPerformanceAddRemoveEnd();
    }

    public static void fillArray(ArrayList<Object> arrayList, LinkedList<Integer> linkedList, int size) {
        for (int i = 0; i < size; i++) {
            linkedList.add(i);
            arrayList.add(i);
        }
    }

    public static void testPerformanceAddRemoveBegin() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();

        start();
        for (int i = 0; i < 120_000; i++) {
            linkedList.add(0, i);
        }
        System.out.println("Begin:");
        System.out.println("LinkedList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 120_000; i++) {
            arrayList.add(0, i);
        }
        System.out.println("arrayList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 120_000; i++) {
            linkedList.remove(0);
        }
        System.out.println("LinkedList.remove(int): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 120_000; i++) {
            arrayList.remove(0);
        }
        System.out.println("arrayList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddRemoveMiddle() {
        ArrayList<Object> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        fillArray(arrayList, linkedList, 100_000);

        start();
        for (int i = 0; i < 100_000; i++) {
            linkedList.add(50_000, i);
        }
        System.out.println("Middle:");
        System.out.println("LinkedList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 100_000; i++) {
            arrayList.add(50_000, i);
        }
        System.out.println("arrayList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 100_000; i++) {
            linkedList.remove(50_000);
        }
        System.out.println("LinkedList.remove(int): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 100_000; i++) {
            arrayList.remove(50_000);
        }
        System.out.println("arrayList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddRemoveEnd() {
        ArrayList<Object> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        fillArray(arrayList, linkedList, 1_000_000);

        start();
        for (int i = 0; i < 1_000_000; i++) {
            linkedList.add(i);
        }
        System.out.println("End:");
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 1_000_000; i++) {
            arrayList.add(i);
        }
        System.out.println("arrayList.add(e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 1_000_000; i++) {
            linkedList.remove(linkedList.size() - 1);
        }
        System.out.println("LinkedList.remove(int): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 1_000_000; i++) {
            arrayList.remove(arrayList.size() - 1);
        }
        System.out.println("arrayList.remove(int): " + getElapsedTime() + " ms");
    }
}
