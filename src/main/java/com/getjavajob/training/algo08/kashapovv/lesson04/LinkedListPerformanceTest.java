package com.getjavajob.training.algo08.kashapovv.lesson04;

import java.util.LinkedList;

import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.start;

/**
 * Created by Вадим on 02.12.2016.
 */
public class LinkedListPerformanceTest {
    public static void main(String[] args) {
        testPerformanceAddRemoveBegin();
        testPerformanceAddRemoveMiddle();
        testPerformanceAddRemoveEnd();
    }

    public static void fillArray(LinkedList<Integer> linkedList, int size) {
        for (int i = 0; i < size; i++) {
            linkedList.add(i);
        }
    }

    public static void testPerformanceAddRemoveBegin() {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();

        start();
        for (int i = 0; i < 5_000_000; i++) {
            linkedList.add(0, i);
        }
        System.out.println("LinkedList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 5_000_000; i++) {
            linkedList.remove(0);
        }
        System.out.println("LinkedList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddRemoveMiddle() {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        fillArray(linkedList, 30_000);

        start();
        for (int i = 0; i < 30_000; i++) {
            linkedList.add(15_000, i);
        }
        System.out.println("LinkedList.add(int, e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 30_000; i++) {
            linkedList.remove(15_000);
        }
        System.out.println("LinkedList.remove(int): " + getElapsedTime() + " ms");
    }

    public static void testPerformanceAddRemoveEnd() {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        fillArray(linkedList, 2_000_000);

        start();
        for (int i = 0; i < 2_000_000; i++) {
            linkedList.add(i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");

        start();
        for (int i = 0; i < 2_000_000; i++) {
            linkedList.remove(linkedList.size() - 1);
        }
        System.out.println("LinkedList.remove(int): " + getElapsedTime() + " ms");
    }
}
