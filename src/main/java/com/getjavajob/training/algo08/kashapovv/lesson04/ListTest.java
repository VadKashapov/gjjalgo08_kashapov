package com.getjavajob.training.algo08.kashapovv.lesson04;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 06.12.2016.
 */
public class ListTest {
    private static List<Integer> list = new ArrayList<>();

    @Test
    public void testAdd() {
        list.add(1);
        list.add(2);
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 5, 1, 2}, list.toArray());
    }

    @Test
    public void testAddAll() {
        List<Integer> addList = Arrays.asList(6, 7, 8);
        list.addAll(addList);
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 5, 6, 7, 8}, list.toArray());
    }

    @Test
    public void testIndexOf() {
        assertEquals("testAdd", 2, list.indexOf(3));
        assertEquals("testAdd", 0, list.indexOf(1));
    }

    @Test
    public void testLastIndexOf() {
        list.add(2);
        assertEquals("testAdd", 5, list.lastIndexOf(2));
    }

    @Test
    public void testListIterator1() {
        ListIterator<Integer> iter = list.listIterator();
        assertEquals("testIterator.hasNext1", true, iter.hasNext());
        assertEquals("testIterator.Next", 1, (int) iter.next());
        assertEquals("testIterator.Next", 2, (int) iter.next());
        iter.remove();
        assertEquals("testIterator.remove", new Integer[]{1, 3, 4, 5}, list.toArray());
    }

    @Test
    public void testListIterator2() {
        ListIterator<Integer> iter = list.listIterator(3);
        assertEquals("testIterator.Next", 4, (int) iter.next());
    }

    @Test
    public void testSubList() {
        assertEquals("testIterator.hasNext", new Integer[]{3, 4}, list.subList(2, 4).toArray());
    }

    @Test
    public void testGet() {
        assertEquals("testGet", 4, (int) list.get(3));
        assertEquals("testGet", 1, (int) list.get(0));
    }

    @Test
    public void testSet() {
        list.set(3, 10);
        assertEquals("testIterator.remove", new Integer[]{1, 2, 3, 10, 5}, list.toArray());
    }

    @Before
    public void setUp() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
    }

    @After
    public void tearDown() {
        list.clear();
    }
}
