package com.getjavajob.training.algo08.kashapovv.lesson04;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Вадим on 03.12.2016.
 */
public class SinglyLinkedList<E> {
    private Node<E> head;

    public void add(E value) {
        if (head == null) {
            head = new Node<>();
            head.value = value;
        } else {
            Node<E> last = head;
            while (last.next != null) {
                last = last.next;
            }
            last.next = new Node<>();
            last.next.value = value;
        }
    }

    public E get(int index) {
        if (index < 0) {
            return null;
        }
        Node<E> element = head;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element.value;
    }

    public int size() {
        Node<E> last = head;
        int size = 0;
        while (last != null) {
            last = last.next;
            size++;
        }
        return size;
    }

    public void reverse() {
        Node<E> tempHead = null;
        Node<E> prevHead = null;
        swap(null, head, prevHead);
        for (; ; ) {
            tempHead = head;
            swap(head.next, head.next.next, prevHead);
            if (head.next == tempHead || head.next.next == tempHead) {
                break;
            }
            prevHead = tempHead;
            tempHead = head;
            swap(head.next, head.next.next, prevHead);
            if (head.next == tempHead || head.next.next == tempHead) {
                break;
            }
            prevHead = tempHead;
        }
    }

    private void swap(Node<E> prev1, Node<E> prev2, Node<E> prevHead) {
        Node<E> node1;
        Node<E> node2 = prev2.next;
        if (prev1 == null) {
            node1 = head;
            node1.next = node2.next;
            node2.next = head;
            head = node2;
        } else {
            node1 = prev1.next;
            prev2.next = head;
            prev1.next = prevHead;
            if (node2 != null) {
                if (node2.next != null) {
                    node1.next = node2.next;
                }
                node2.next = node1;
                head = node2;
            } else {
                head = node1;
            }
        }
    }

    public List<E> asList() {
        List<E> list = new LinkedList<>();
        Node<E> element = head;
        while (element != null) {
            list.add(element.value);
            element = element.next;
        }
        return list;
    }

    protected static class Node<E> {
        Node<E> next;
        E value;
    }
}
