package com.getjavajob.training.algo08.kashapovv.lesson04;

import static com.getjavajob.training.algo08.kashapovv.Utils.Assert.assertEquals;

/**
 * Created by Вадим on 03.12.2016.
 */
public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testAdd();
        testGet();
        testSize();
        testReverse();
    }

    public static void testAdd() {
        SinglyLinkedList<Integer> singlyList = new SinglyLinkedList<>();
        singlyList.add(1);
        singlyList.add(2);
        singlyList.add(3);

        assertEquals("SinglyLinkedListTest.testAdd", new Integer[]{1, 2, 3},
                singlyList.asList().toArray());
    }

    public static void testGet() {
        SinglyLinkedList<Integer> singlyList = new SinglyLinkedList<>();
        singlyList.add(1);
        singlyList.add(2);
        singlyList.add(3);
        singlyList.add(4);

        assertEquals("SinglyLinkedListTest.testGet", "3", singlyList.get(2).toString());
    }

    public static void testSize() {
        SinglyLinkedList<Integer> singlyList = new SinglyLinkedList<>();
        singlyList.add(1);
        singlyList.add(2);
        singlyList.add(3);
        singlyList.add(4);

        assertEquals("SinglyLinkedListTest.testSize", 4, singlyList.size());
    }

    public static void testReverse() {
        SinglyLinkedList<Integer> singlyList = new SinglyLinkedList<>();

        assertEquals("SinglyLinkedListTest.testReverse.Empty", new Integer[]{},
                singlyList.asList().toArray());

        singlyList.add(1);

        assertEquals("SinglyLinkedListTest.testReverse.One", new Integer[]{1},
                singlyList.asList().toArray());

        singlyList.add(2);
        singlyList.add(3);
        singlyList.add(4);
        singlyList.reverse();

        assertEquals("SinglyLinkedListTest.testReverse.Even", new Integer[]{4, 3, 2, 1},
                singlyList.asList().toArray());

        singlyList.add(5);
        singlyList.reverse();

        assertEquals("SinglyLinkedListTest.testReverse.Odd", new Integer[]{5, 1, 2, 3, 4},
                singlyList.asList().toArray());
    }
}


