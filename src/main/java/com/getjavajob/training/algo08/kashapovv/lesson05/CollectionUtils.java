package com.getjavajob.training.algo08.kashapovv.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Вадим on 09.12.2016.
 */

interface Closure<E> {
    void execute(E e);
}

interface Filter<E> {
    boolean isValid(E e);
}

interface Transform<E> {
    E transform(E e);
}

public class CollectionUtils {

    public static <I> boolean filter(Collection<I> inputs, Filter filter) {
        if (inputs == null || filter == null) {
            throw new IllegalArgumentException("Invalid collection or filter");
        }

        int beforeSize = inputs.size();
        Iterator<I> iterator = inputs.iterator();
        while (iterator.hasNext()) {
            if (!filter.isValid(iterator.next())) {
                iterator.remove();
            }
        }
        return inputs.size() != beforeSize;
    }

    public static <I> Collection<I> filterNewCol(Collection<I> inputs, Filter filter) {
        if (inputs == null || filter == null) {
            throw new IllegalArgumentException("Invalid collection or filter");
        }
        Collection<I> results = new ArrayList<>();
        for (I input : inputs) {
            if (filter.isValid(input)) {
                results.add(input);
            }
        }
        return results;
    }

    public static <I> Collection<I> transformNewCol(Collection<I> inputs,
                                                    Transform transformer) {
        if (inputs == null || transformer == null) {
            throw new IllegalArgumentException("Invalid collection or transformer");
        }
        Collection<I> results = new ArrayList<>();
        for (I input : inputs) {
            results.add((I) transformer.transform(input));
        }
        return results;
    }

    public static <I> void transform(Collection<I> inputs, Transform transformer) {
        if (inputs == null || transformer == null) {
            throw new IllegalArgumentException("Invalid collection or transformer");
        }
        Collection<I> results = new ArrayList<>();
        for (I input : inputs) {
            results.add((I) transformer.transform(input));
        }
        inputs.clear();
        inputs.addAll(results);
    }

    public static <E> void forAllDo(Collection<E> inputs, Closure closure) {
        if (inputs == null || closure == null) {
            throw new IllegalArgumentException("Invalid collection or closure");
        }
        for (E input : inputs) {
            closure.execute(input);
        }
    }

    public static <T> Collection<T> unmodifiableCollection(Collection<? extends T> inputs) {
        if (inputs == null) {
            throw new IllegalArgumentException("Invalid collection");
        }
        return new UnmodifiableCollection<>(inputs);
    }

    private static class UnmodifiableCollection<E> implements Collection<E> {
        private Collection<? extends E> inputs;

        UnmodifiableCollection(Collection<? extends E> inputs) {
            this.inputs = inputs;
        }

        public int size() {
            return inputs.size();
        }

        public boolean isEmpty() {
            return inputs.isEmpty();
        }

        public boolean contains(Object o) {
            return inputs.contains(o);
        }

        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> i = inputs.iterator();

                public boolean hasNext() {
                    return i.hasNext();
                }

                public E next() {
                    return i.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public Object[] toArray() {
            return inputs.toArray();
        }

        public <T> T[] toArray(T[] a) {
            return inputs.toArray(a);
        }

        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        public boolean containsAll(Collection<?> c) {
            return inputs.containsAll(c);
        }

        public boolean addAll(Collection<? extends E> c) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }

        public String toString() {
            return inputs.toString();
        }
    }
}
