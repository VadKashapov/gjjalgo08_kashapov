package com.getjavajob.training.algo08.kashapovv.lesson05;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 09.12.2016.
 */
public class CollectionUtilsTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    Collection<Employee> employees;

    @Before
    public void setUp() throws Exception {
        employees = new ArrayList<>(asList(
                new Employee("Igor", "Volnov", 1200),
                new Employee("Dmitriy", "Lublinov", 1200),
                new Employee("Ivan", "Volkov", 1200),
                new Employee("Sergey", "Volkov", 1200),
                new Employee("Andrey", "Petrov", 1200),
                new Employee("Michael", "Petrov", 1200)));
    }

    @After
    public void tearDown() throws Exception {
        employees.clear();
    }

    @Test
    public void filter() throws Exception {
        assertTrue("testFilter1", CollectionUtils.filter(employees,
                new Filter() {
                    public boolean isValid(Object o) {
                        String keyword = "Volkov";

                        Employee employee = (Employee) o;
                        return employee.toString().toLowerCase().contains(keyword.toLowerCase());
                    }
                }));
        assertEquals("testFilter1",
                asList(
                        new Employee("Ivan", "Volkov", 1200),
                        new Employee("Sergey", "Volkov", 1200))
                , employees);

        thrown.expect(IllegalArgumentException.class);
        CollectionUtils.filter(null, null);
    }

    @Test
    public void filter2() throws Exception {
        assertEquals("testFilter1"
                , asList(
                        new Employee("Ivan", "Volkov", 1200),
                        new Employee("Sergey", "Volkov", 1200))
                , CollectionUtils.filterNewCol(employees,
                        new Filter() {
                            public boolean isValid(Object o) {
                                String keyword = "Volkov";

                                Employee employee = (Employee) o;
                                return employee.toString().toLowerCase().contains(keyword.toLowerCase());
                            }
                        }));

        thrown.expect(IllegalArgumentException.class);
        CollectionUtils.filterNewCol(null, null);
    }

    @Test
    public void transform() throws Exception {
        assertEquals("testTransform"
                , asList("Volnov", "Lublinov", "Volkov", "Volkov", "Petrov", "Petrov")
                , CollectionUtils.transformNewCol(employees, new Transform() {
                    public Object transform(Object o) {
                        Employee employee = (Employee) o;
                        return employee.lastName;
                    }
                }));

        thrown.expect(IllegalArgumentException.class);
        CollectionUtils.transformNewCol(null, null);
    }

    @Test
    public void transform2() throws Exception {
        CollectionUtils.transform(employees, new Transform() {
            public Object transform(Object o) {
                return ((Employee) o).lastName;
            }
        });

        assertEquals("testTransform", asList("Volnov", "Lublinov", "Volkov", "Volkov", "Petrov", "Petrov"), employees);

        thrown.expect(IllegalArgumentException.class);
        CollectionUtils.transform(null, null);
    }

    @Test
    public void forAllDo() throws Exception {
        CollectionUtils.forAllDo(employees, new Closure() {
            public void execute(Object o) {
                Employee employee = (Employee) o;
                assert employee != null;
                employee.setSalary(employee.getSalary() + 400);
            }
        });
        Collection<Employee> expected = Arrays.asList(
                new Employee("Igor", "Volnov", 1600),
                new Employee("Dmitriy", "Lublinov", 1600),
                new Employee("Ivan", "Volkov", 1600),
                new Employee("Sergey", "Volkov", 1600),
                new Employee("Andrey", "Petrov", 1600),
                new Employee("Michael", "Petrov", 1600));

        assertEquals("testForAllDo", expected, employees);

        thrown.expect(IllegalArgumentException.class);
        CollectionUtils.forAllDo(null, null);
    }

    @Test
    public void unmodifiableCollection() throws Exception {
        Collection<Employee> unmodifiableEmployees = CollectionUtils.unmodifiableCollection(employees);
        Collection<Employee> expected = Arrays.asList(
                new Employee("Igor", "Volnov", 1200),
                new Employee("Dmitriy", "Lublinov", 1200),
                new Employee("Ivan", "Volkov", 1200),
                new Employee("Sergey", "Volkov", 1200),
                new Employee("Andrey", "Petrov", 1200),
                new Employee("Michael", "Petrov", 1200));

        assertEquals("testUnmodifiable", true, Arrays.equals(expected.toArray(), unmodifiableEmployees.toArray()));

        thrown.expect(UnsupportedOperationException.class);
        unmodifiableEmployees.add(new Employee("1", "2", 3) {
        });

    }

    @Test
    public void unmodifiableCollectionException1() {
        thrown.expect(IllegalArgumentException.class);
        CollectionUtils.unmodifiableCollection(null);
    }

    private class Employee {
        private String firstName;
        private String lastName;
        private int salary;

        Employee(String firstName, String lastName, int salary) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.salary = salary;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (firstName != null ? firstName.hashCode() : 0);
            result = prime * result + (lastName != null ? lastName.hashCode() : 0);
            result = prime * result + salary;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Employee emp = (Employee) obj;
            return !(!firstName.equals(emp.firstName)
                    || !lastName.equals(emp.lastName)
                    || salary != emp.salary);
        }

        @Override
        public String toString() {
            return "" + firstName + " " +
                    lastName + " " +
                    salary;

        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }
    }
}