package com.getjavajob.training.algo08.kashapovv.lesson05;

import com.getjavajob.training.algo08.kashapovv.Utils.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 06.12.2016.
 */

public class DequeTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    Deque<Integer> deque;

    @Before
    public void setUp() throws Exception {
        deque = new ArrayDeque<>();
        deque.add(1);
        deque.add(2);
        deque.add(3);
        deque.add(4);
    }

    @After
    public void tearDown() throws Exception {
        deque.clear();
    }

    @Test
    public void addFirst() throws Exception {
        deque.addFirst(5);
        deque.addFirst(6);
        Assert.assertEquals("testAddFirst", Arrays.asList(6, 5, 1, 2, 3, 4).toArray(), deque.toArray());
    }

    @Test
    public void addLast() throws Exception {
        deque.addLast(5);
        deque.addLast(6);
        deque.addLast(7);
        Assert.assertEquals("testAddLast", Arrays.asList(1, 2, 3, 4, 5, 6, 7).toArray(), deque.toArray());
    }

    @Test
    public void offerFirst() throws Exception {
        deque.offerFirst(5);
        deque.offerFirst(6);
        Assert.assertEquals("testOfferFirst", Arrays.asList(6, 5, 1, 2, 3, 4).toArray(), deque.toArray());
    }

    @Test
    public void offerLast() throws Exception {
        deque.addLast(5);
        deque.addLast(6);
        Assert.assertEquals("testAddFirst", Arrays.asList(1, 2, 3, 4, 5, 6).toArray(), deque.toArray());
    }

    @Test
    public void removeFirst() throws Exception {
        deque.removeFirst();
        deque.removeFirst();
        assertEquals("testRemoveFirst", new Integer[]{3, 4}, deque.toArray());
        thrown.expect(NoSuchElementException.class);
        deque.removeFirst();
        deque.removeFirst();
        deque.removeFirst();
    }

    @Test
    public void removeLast() throws Exception {
        deque.removeLast();
        deque.removeLast();
        assertEquals("testRemoveLast", new Integer[]{1, 2}, deque.toArray());
        thrown.expect(NoSuchElementException.class);
        deque.removeLast();
        deque.removeLast();
        deque.removeLast();
    }

    @Test
    public void pollFirst() throws Exception {
        deque.pollFirst();
        deque.pollFirst();
        assertEquals(new Integer[]{3, 4}, deque.toArray());
        deque.pollFirst();
        deque.pollFirst();
        assertEquals("testPollFirst", null, deque.pollFirst());
    }

    @Test
    public void pollLast() throws Exception {
        deque.pollLast();
        deque.pollLast();
        assertEquals("testPollLast", new Integer[]{1, 2}, deque.toArray());
        deque.pollLast();
        deque.pollLast();
        assertEquals("testPollLast", null, deque.pollLast());
    }

    @Test
    public void getFirst() throws Exception {
        assertEquals("testGetFirst", 1, (int) deque.getFirst());
        deque.clear();
        thrown.expect(NoSuchElementException.class);
        deque.getFirst();
    }

    @Test
    public void getLast() throws Exception {
        assertEquals("testGetLast", 4, (int) deque.getLast());
        deque.clear();
        thrown.expect(NoSuchElementException.class);
        deque.getLast();
    }

    @Test
    public void peekFirst() throws Exception {
        deque.addFirst(10);
        assertEquals("testPeekFirst", 10, (int) deque.peekFirst());
    }

    @Test
    public void peekLast() throws Exception {
        deque.addLast(10);
        assertEquals("testPeekFirst", 10, (int) deque.peekLast());
    }

    @Test
    public void removeFirstOccurrence() throws Exception {
        deque.removeFirstOccurrence(4);
        deque.removeFirstOccurrence(2);
        assertEquals("testRemoveFirstOccurrence", new Integer[]{1, 3}, deque.toArray());
    }

    @Test
    public void removeLastOccurrence() throws Exception {
        deque.removeLastOccurrence(2);
        deque.removeLastOccurrence(4);
        assertEquals("testRemoveLastOccurrence", new Integer[]{1, 3}, deque.toArray());
    }

    @Test
    public void add() throws Exception {
        deque.add(10);
        deque.add(11);
        deque.add(12);
        deque.add(13);
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 10, 11, 12, 13}, deque.toArray());
    }

    @Test
    public void offer() throws Exception {
        deque.offer(10);
        deque.offer(11);
        deque.offer(12);
        deque.offer(13);
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 10, 11, 12, 13}, deque.toArray());
    }

    @Test
    public void remove() throws Exception {
        deque.remove();
        deque.remove();
        assertEquals("testAdd", new Integer[]{3, 4}, deque.toArray());
    }

    @Test
    public void poll() throws Exception {
        deque.poll();
        deque.poll();
        deque.poll();
        assertEquals("testPoll", new Integer[]{4}, deque.toArray());
        deque.poll();
        assertEquals("testPoll", null, deque.poll());
    }

    @Test(expected = NoSuchElementException.class)
    public void element() throws Exception {
        assertEquals("testElement", 1, (int) deque.element());
        assertEquals("testElement", new Integer[]{1, 2, 3, 4}, deque.toArray());
        deque.clear();
        deque.element();
    }

    @Test
    public void peek() throws Exception {
        assertEquals("testPeek", 1, (int) deque.peek());
        assertEquals("testPeek", new Integer[]{1, 2, 3, 4}, deque.toArray());
        deque.clear();
        assertEquals("testPeek", null, deque.peek());
    }

    @Test
    public void push() throws Exception {
        deque.push(5);
        assertEquals("testPush", new Integer[]{5, 1, 2, 3, 4}, deque.toArray());
    }

    @Test
    public void pop() throws Exception {
        assertEquals("testPop", 1, (int) deque.pop());
    }

    @Test
    public void size() throws Exception {
        assertEquals("testSize", 4, deque.size());
    }

    @Test
    public void isEmpty() throws Exception {
        assertEquals("testIsEmpty", false, deque.isEmpty());
        deque.clear();
        assertEquals("testIsEmpty", true, deque.isEmpty());
    }

    @Test
    public void iterator() throws Exception {
        Iterator iterator = deque.iterator();
        assertEquals("testIterator", 1, iterator.next());
        assertEquals("testIterator", 2, iterator.next());
        iterator.remove();
        assertEquals("testIterator", 3, iterator.next());
        assertEquals("testIterator", 4, iterator.next());
        iterator.remove();
        assertEquals("testIterator", new Integer[]{1, 3}, deque.toArray());
    }

    @Test
    public void descendingIterator() throws Exception {
        Iterator descIterator = deque.descendingIterator();
        assertEquals("testDescIterator", 4, descIterator.next());
        assertEquals("testDescIterator", 3, descIterator.next());
        descIterator.remove();

        assertEquals("testDescIterator", 2, descIterator.next());
        assertEquals("testDescIterator", 1, descIterator.next());
        descIterator.remove();
        assertEquals("testDescIterator", new Integer[]{2, 4}, deque.toArray());
    }

    @Test
    public void contains() throws Exception {
        assertEquals("testContains", true, deque.contains(4));
        assertEquals("testContains", false, deque.contains(5));
    }

    @Test
    public void remove1() throws Exception {
        assertEquals("testRemove", true, deque.remove(1));
        assertEquals("testRemove", true, deque.remove(2));
        assertEquals("testRemove", false, deque.remove(5));
        assertEquals("testRemove", new Integer[]{3, 4}, deque.toArray());
    }
}
