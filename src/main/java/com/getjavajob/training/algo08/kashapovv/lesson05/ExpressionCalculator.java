package com.getjavajob.training.algo08.kashapovv.lesson05;

import java.util.InputMismatchException;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Character.isDigit;

/**
 * Created by Вадим on 06.12.2016.
 */
public class ExpressionCalculator {

    public <E> double calculateExpression(String expression) {
        Queue<E> queueExpression = toPostfixNotationQueue(expression);
        Stack<Double> stack = new LinkedListStack<>();
        while (!queueExpression.isEmpty()) {
            E token = queueExpression.remove();
            if (token instanceof Double) {
                stack.push((Double) token);
            } else {
                switch ((Character) token) {
                    case '+':
                        stack.push(stack.pop() + stack.pop());
                        break;
                    case '-':
                        stack.push(-stack.pop() + stack.pop());
                        break;
                    case '/':
                        stack.push(1 / stack.pop() * stack.pop());
                        break;
                    case '*':
                        stack.push(stack.pop() * stack.pop());
                        break;
                }
            }
        }
        return stack.pop();
    }

    private Queue toPostfixNotationQueue(String expression) throws InputMismatchException {
        if (expression == null) {
            throw new IllegalArgumentException("Invalid string instance");
        }
        Queue queueExpression = new LinkedListQueue<>();
        Stack<Character> stack = new LinkedListStack<>();

        expression = expression.replaceAll("\\s+", "");
        parseExpression(expression);
        Pattern pattern = Pattern.compile("(\\d+(\\.\\d+)?|[-,+,*,/,(,)])");
        Matcher matcher = pattern.matcher(expression);

        while (matcher.find()) {
            String token = matcher.group(1);
            char tokenChar = token.charAt(0);

            if (isDigit(tokenChar)) {
                queueExpression.add(tryParse(token));
            } else if (isOperator(tokenChar)) {
                retrieveLargerEqualRankOperators(queueExpression, stack, tokenChar);
                stack.push(tokenChar);
            } else if (tokenChar == '(') {
                stack.push(tokenChar);
            } else if (tokenChar == ')') {
                retrieveBraceExpression(queueExpression, stack);
            }
        }
        retrieveStackRemains(queueExpression, stack);
        return queueExpression;
    }

    private void retrieveLargerEqualRankOperators(Queue queueExpression, Stack<Character> stack, char tokenChar) {
        while (!stack.empty()) {
            if (getRank(tokenChar) <= getRank(stack.peek())) {
                queueExpression.add(stack.pop());
            } else {
                break;
            }
        }
    }

    private void retrieveStackRemains(Queue queueExpression, Stack<Character> stack) {
        while (!stack.empty()) {
            if ("()".indexOf(stack.peek()) >= 0) {
                throw new InputMismatchException("Parentheses mismatch");
            }
            queueExpression.add(stack.pop());
        }
    }

    private void retrieveBraceExpression(Queue queueExpression, Stack<Character> stack) {
        while (!stack.empty() && stack.peek() != '(') {
            queueExpression.add(stack.pop());
        }
        if (!stack.empty() && stack.peek() == '(') {
            stack.pop();
        } else {
            throw new InputMismatchException("Parentheses mismatch");
        }
    }

    private boolean parseExpression(String expression) {
        if (isOperator(expression.charAt(0)) || isOperator(expression.charAt(expression.length() - 1))) {
            throw new InputMismatchException("Invalid expression");
        }
        for (int i = 0; i < expression.length() - 1; i++) {
            if (isDigit(expression.charAt(i))) {
                i = parseNumber(expression, i) - 1;
            } else if (isOperator(expression.charAt(i)) && expression.length() >= i + 1
                    && (expression.charAt(i + 1) != '(' && !isDigit(expression.charAt(i + 1)))) {
                throw new InputMismatchException("Invalid expression");
            } else if (expression.charAt(i) == '(' && expression.length() >= i + 1
                    && (expression.charAt(i + 1) != '(' && !isDigit(expression.charAt(i + 1)))) {
                throw new InputMismatchException("Invalid expression");
            } else if (expression.charAt(i) == ')' && expression.length() < i + 1
                    && (expression.charAt(i + 1) != ')' && !isOperator(expression.charAt(i + 1)))) {
                throw new InputMismatchException("Invalid expression");
            }
        }
        return true;
    }

    private int parseNumber(String expression, int i) {
        int pointCount = 0;
        while (i < expression.length() - 1 && (isDigit(expression.charAt(i)) ^ expression.charAt(i) == '.')) {
            if (expression.charAt(i) == '.') {
                pointCount++;
            }
            i++;
        }
        if (pointCount > 1 || (!isDigit(expression.charAt(i)) &&
                expression.charAt(i) != ')' && !isOperator(expression.charAt(i)))) {
            throw new InputMismatchException("Invalid expression");
        }
        return i;
    }

    public String toPostfixNotation(String expression) {
        Queue queue = toPostfixNotationQueue(expression);
        StringBuilder builder = new StringBuilder(expression.length());
        while (!queue.isEmpty()) {
            builder.append(queue.remove()).append(' ');
        }
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    private Double tryParse(String number) {
        return Double.valueOf(number);
    }

    private boolean isOperator(char symbol) {
        return "-+/*".indexOf(symbol) >= 0;
    }

    private int getRank(char symbol) {
        switch (symbol) {
            case '/':
            case '*':
                return 12;
            case '+':
            case '-':
                return 11;
            default:
                return 0;
        }
    }
}