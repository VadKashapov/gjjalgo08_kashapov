package com.getjavajob.training.algo08.kashapovv.lesson05;

import com.getjavajob.training.algo08.kashapovv.Utils.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.InputMismatchException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 07.12.2016.
 */
public class ExpressionCalculatorTest {
    private static final double DELTA = 1e-3;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    private ExpressionCalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new ExpressionCalculator();
    }

    @Test
    public void calculateExpression() throws Exception {
        assertEquals("testCalculateExpression", 7.0, calculator.calculateExpression("5+((1+2)*4)-10"), DELTA);
        assertEquals("testCalculateExpression", 20.0, calculator.calculateExpression("(10.50 - 0.5) / 0.5"), DELTA);
        assertEquals("testCalculateExpression", 20.0,
                calculator.calculateExpression(" ( 5.5 -   0.5 )  /  0.25 "), DELTA);
        assertEquals("testCalculateExpression", 0.5, calculator.calculateExpression("10/2/2/5"), DELTA);

        thrown.expect(IllegalArgumentException.class);
        calculator.calculateExpression(null);
    }

    @Test
    public void calculateExpressionException12() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(. - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException11() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(.50 - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException10() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.5FSD0 - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException9() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.5.0 - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException8() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.50 - 0.5)( / 0.5");
    }

    @Test
    public void calculateExpressionException7() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("()10.50 - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException6() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression(")(10.50 - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException5() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.50 - 0.5) / 0.5(");
    }

    @Test
    public void calculateExpressionException4() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.50 - 0.5) / 0.5+");
    }

    @Test
    public void calculateExpressionException3() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.50 - 0.5+) / 0.5");
    }

    @Test
    public void calculateExpressionException2() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("-(10.50 - 0.5) / 0.5");
    }

    @Test
    public void calculateExpressionException1() {
        thrown.expect(InputMismatchException.class);
        calculator.calculateExpression("(10.50 - 0.5) // 0.5");
    }

    @Test()
    public void toPostfixNotation() throws Exception {
        assertEquals("testToPostfixNotation", "5.0 1.0 2.0 + 4.0 * + 3.0 -",
                calculator.toPostfixNotation("5+((1+2)*4)-3"));
        try {
            assertEquals("testToPostfixNotation", "5.0 1.0 2.0 + 4.0 * + 3.0 -",
                    calculator.toPostfixNotation("5+*3523/32#@$32"));
            Assert.fail("Exception not thrown");
        } catch (InputMismatchException e) {
            assertEquals("testToPostfixNotation", "Invalid expression", e.getMessage());
        }
    }
}