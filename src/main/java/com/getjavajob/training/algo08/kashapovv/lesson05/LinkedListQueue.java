package com.getjavajob.training.algo08.kashapovv.lesson05;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Вадим on 04.12.2016.
 */


public class LinkedListQueue<E> extends AbstractQueue<E> {
    private Node<E> head;

    @Override
    public boolean add(E value) {
        if (head == null) {
            head = new Node<>();
            head.value = value;
        } else {
            Node<E> tail = head;
            while (tail.next != null) {
                tail = tail.next;
            }
            tail.next = new Node<>();
            tail.next.value = value;
        }
        return true;
    }

    @Override
    public E remove() {
        if (head == null) {
            return null;
        }
        Node<E> temp = head;
        if (head.next != null) {
            head = head.next;
            temp.next = null;
        } else {
            temp = head;
            head = null;
        }
        return temp.value;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    public static class Node<E> {
        Node<E> next;
        E value;
    }
}

abstract class AbstractQueue<E> implements java.util.Queue<E> {

    public int size() {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    public Iterator iterator() {
        throw new UnsupportedOperationException();
    }

    public E[] toArray() {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public boolean offer(E o) {
        throw new UnsupportedOperationException();
    }

    public E poll() {
        throw new UnsupportedOperationException();
    }

    public E element() {
        throw new UnsupportedOperationException();
    }

    public E peek() {
        throw new UnsupportedOperationException();
    }
}