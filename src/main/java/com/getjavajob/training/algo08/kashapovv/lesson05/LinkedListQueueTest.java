package com.getjavajob.training.algo08.kashapovv.lesson05;

import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 06.12.2016.
 */
public class LinkedListQueueTest {
    private static Queue<Integer> queue = new LinkedListQueue<>();

    @Test
    public void addRemove() throws Exception {
        queue.add(null);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        assertEquals("testRemove", null, queue.remove());
        assertEquals("testRemove", 1, (int) queue.remove());
        assertEquals("testRemove", 2, (int) queue.remove());
        assertEquals("testRemove", 3, (int) queue.remove());
        assertEquals("testRemove", 4, (int) queue.remove());
    }
}