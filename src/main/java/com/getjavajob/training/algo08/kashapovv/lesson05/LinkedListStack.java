package com.getjavajob.training.algo08.kashapovv.lesson05;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Вадим on 04.12.2016.
 */

interface Stack<E> {
    void push(E e);

    E pop();

    E peek();

    boolean empty();
}

public class LinkedListStack<E> implements Stack<E> {
    private SinglyLinkedList<E> list;
    private int size;

    public LinkedListStack() {
        this.list = new SinglyLinkedList<>();
    }

    public void push(E e) {
        list.add(e);
        size++;
    }

    public E pop() {
        size--;
        return list.remove();
    }

    public boolean empty() {
        return size == 0;
    }

    public E peek() {
        return list.get(0);
    }
}

class SinglyLinkedList<E> {
    private Node<E> head;

    public void add(E value) {
        if (head == null) {
            head = new Node<>();
            head.value = value;
        } else {
            Node<E> newElement = new Node<>();
            newElement.value = value;
            newElement.next = head;
            head = newElement;
        }
    }

    public E remove() {
        if (head == null) {
            return null;
        }
        Node<E> temp = head;
        head = head.next;
        temp.next = null;
        return temp.value;
    }

    public E get(int index) {
        if (index < 0) {
            return null;
        }
        Node<E> element = head;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element.value;
    }

    public int size() {
        Node<E> last = head;
        int size = 0;
        while (last != null) {
            last = last.next;
            size++;
        }
        return size;
    }

    public void reverse() {
        Node<E> curr = head;
        if (head != null && head.next != null) {
            head = head.next;
            reverse();
        } else {
            return;
        }
        swap(curr, curr.next);
    }

    private void swap(Node<E> node1, Node<E> node2) {
        node2.next = node1;
        node1.next = null;
    }

    public List<E> asList() {
        List<E> list = new LinkedList<>();
        Node<E> element = head;
        while (element != null) {
            list.add(element.value);
            element = element.next;
        }
        return list;
    }

    protected static class Node<E> {
        Node<E> next;
        E value;
    }
}