package com.getjavajob.training.algo08.kashapovv.lesson05;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 06.12.2016.
 */
public class LinkedListStackTest {
    private static Stack<Integer> stack = new LinkedListStack<>();

    @Test
    public void pushPop() throws Exception {
        stack.push(null);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        List<Integer> list = new ArrayList<>();
        list.add(stack.pop());
        assertEquals("testPushPop1", new Integer[]{4}, list.toArray());
        list.add(stack.pop());
        assertEquals("testPushPop2", new Integer[]{4, 3}, list.toArray());
        list.add(stack.pop());
        assertEquals("testPushPop3", new Integer[]{4, 3, 2}, list.toArray());
        list.add(stack.pop());
        assertEquals("testPushPop4", new Integer[]{4, 3, 2, 1}, list.toArray());
        list.add(stack.pop());
        assertEquals("testPushPop4", new Integer[]{4, 3, 2, 1, null}, list.toArray());
    }
}