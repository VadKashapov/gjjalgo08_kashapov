package com.getjavajob.training.algo08.kashapovv.lesson05;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 06.12.2016.
 */
public class QueueTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    Queue<Integer> queue;

    @Before
    public void setUp() throws Exception {
        queue = new ArrayDeque<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
    }

    @After
    public void tearDown() throws Exception {
        queue.clear();
    }

    @Test
    public void add() throws Exception {
        queue.add(10);
        queue.add(11);
        queue.add(12);
        queue.add(13);
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 10, 11, 12, 13}, queue.toArray());
    }

    @Test
    public void offer() throws Exception {
        queue.offer(10);
        queue.offer(11);
        queue.offer(12);
        queue.offer(13);
        assertEquals("testAdd", new Integer[]{1, 2, 3, 4, 10, 11, 12, 13}, queue.toArray());
    }

    @Test
    public void remove() throws Exception {
        assertEquals("testRemove", 1, (int) queue.remove());
        assertEquals("testRemove", 2, (int) queue.remove());
        assertEquals("testRemove", 3, (int) queue.remove());
        assertEquals("testRemove", 4, (int) queue.remove());
        assertEquals("testRemove", new Integer[]{}, queue.toArray());
    }

    @Test
    public void poll() throws Exception {
        queue.poll();
        queue.poll();
        queue.poll();
        assertEquals("testPoll", new Integer[]{4}, queue.toArray());
        queue.poll();
        assertEquals("testPoll", null, queue.poll());
    }

    @Test(expected = NoSuchElementException.class)
    public void element() throws Exception {
        assertEquals("testElement", 1, (int) queue.element());
        assertEquals("testElement", new Integer[]{1, 2, 3, 4}, queue.toArray());
        queue.clear();
        queue.element();
    }

    @Test
    public void peek() throws Exception {
        assertEquals("testPeek", 1, (int) queue.peek());
        assertEquals("testPeek", new Integer[]{1, 2, 3, 4}, queue.toArray());
        queue.clear();
        assertEquals("testPeek", null, queue.peek());
    }

    @Test
    public void size() throws Exception {
        assertEquals("testSize", 4, queue.size());
    }

    @Test
    public void isEmpty() throws Exception {
        assertEquals("testIsEmpty", false, queue.isEmpty());
        queue.clear();
        assertEquals("testIsEmpty", true, queue.isEmpty());
    }

    @Test
    public void iterator() throws Exception {
        Iterator iterator = queue.iterator();
        assertEquals("testIterator", 1, iterator.next());
        iterator.remove();
        assertEquals("testIterator", 2, iterator.next());
        assertEquals("testIterator", 3, iterator.next());
        assertEquals("testIterator", 4, iterator.next());
        iterator.remove();
        assertEquals("testIterator", new Integer[]{2, 3}, queue.toArray());
    }

    @Test
    public void contains() throws Exception {
        assertEquals("testContains", true, queue.contains(4));
        assertEquals("testContains", false, queue.contains(5));
    }

    @Test
    public void remove1() throws Exception {
        assertEquals("testRemove", true, queue.remove(1));
        assertEquals("testRemove", true, queue.remove(2));
        assertEquals("testRemove", false, queue.remove(5));
        assertEquals("testRemove", new Integer[]{3, 4}, queue.toArray());
    }
}
