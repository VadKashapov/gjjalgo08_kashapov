package com.getjavajob.training.algo08.kashapovv.lesson06;

/**
 * Created by Вадим on 10.12.2016.
 */

public class AssociativeArray<K, V> {
    private static final int DEFAULT_CAPACITY = 0;
    private static final int DEFAULT_SIZE = 16;
    private static final int EXPAND_SIZE_ON = 2;

    private Entry<K, V>[] entries;
    private int capacity;

    AssociativeArray() {
        capacity = DEFAULT_CAPACITY;
        entries = new Entry[DEFAULT_SIZE];
    }

    public V add(K key, V value) {
        checkForResize();
        int hash = getHash(key);
        int index = getIndex(hash, entries.length);
        Entry<K, V> entry = entries[index];
        Entry<K, V> prev = entry;
        while (entry != null) {
            Object k;
            if (entry.hash == hash && ((k = entry.key) == key || key.equals(k))) {
                V oldValue = entry.value;
                entry.value = value;
                return oldValue;
            }
            prev = entry;
            entry = entry.next;
        }
        if (prev == null) {
            entries[index] = new Entry<>(key, value, null, hash);
        } else {
            prev.next = new Entry<>(key, value, null, hash);
        }
        capacity++;
        return null;
    }

    public V get(K key) {
        Entry<K, V> entry = getEntry(key);
        return null == entry ? null : entry.value;
    }

    private Entry<K, V> getEntry(K key) {
        int hash = getHash(key);
        for (Entry<K, V> entry = entries[getIndex(hash, entries.length)];
             entry != null;
             entry = entry.next) {

            Object k;
            if (entry.hash == hash &&
                    ((k = entry.key) == key || (key != null && key.equals(k)))) {
                return entry;
            }
        }
        return null;
    }

    private int getIndex(int hash, int length) {
        return hash & length - 1;
    }

    private int getHash(K key) {
        if (key == null) {
            throw new IllegalArgumentException("Unsupported key instance");
        }
        return key.hashCode();
    }

    private void checkForResize() {
        if (capacity == entries.length) {
            rehash(entries.length * EXPAND_SIZE_ON);
        }
    }

    private void rehash(int newLength) {
        Entry<K, V>[] newEntries = new Entry[newLength];
        for (Entry<K, V> entry : entries) {
            if (entry != null) {
                newEntries[entry.hash & newLength - 1] = entry;
            }
        }
        entries = newEntries;
    }

    public V remove(K key) {
        int index = getIndex(getHash(key), entries.length);
        Entry<K, V> entry = entries[index];
        if (entry == null) {
            return null;
        } else {
            Entry<K, V> prevEntry = entry;
            while (entry.next != null && !entry.key.equals(key)) {
                prevEntry = entry;
                entry = entry.next;
            }
            if (entry.key.equals(key)) {
                V oldValue = entry.value;
                if (entry.next != null) {
                    prevEntry.next = entry.next;
                } else {
                    prevEntry.next = null;
                }
                if (entries[index] == entry) {
                    if (entry.next != null) {
                        entries[index] = entry.next;
                    } else {
                        entries[index] = null;
                    }
                }
                capacity--;
                return oldValue;
            }
            return null;
        }
    }

    public Entry<K, V>[] toArray() {
        return entries;
    }

    private static class Entry<K, V> {
        K key;
        V value;
        Entry<K, V> next;
        int hash;

        Entry(K key, V value, Entry<K, V> next, int hash) {
            this.key = key;
            this.value = value;
            this.next = next;
            this.hash = hash;
        }
    }
}
