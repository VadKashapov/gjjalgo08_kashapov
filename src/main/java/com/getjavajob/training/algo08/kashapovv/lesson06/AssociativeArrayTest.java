package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 10.12.2016.
 */
public class AssociativeArrayTest {
    private AssociativeArray<String, String> map;

    @Test
    public void add() throws Exception {
        map = new AssociativeArray<>();

        map.add("A", "1");
        map.add("B", "2");
        map.add("C", "3");
        map.add("D", "4");
        map.add("E", "5");
        map.add("F", "6");
        map.add("G", "7");
        map.add("H", "8");
        map.add("I", "9");
        map.add("J", "10"); // reached capacity limit
        map.add("K", "11");
    }

    @Test
    public void get() throws Exception {
        map = new AssociativeArray<>();

        map.add("A", "1");
        map.add("B", "2");
        map.add("C", "3");
        map.add("D", "4");
        map.add("E", "5");
        map.add("F", "6");
        map.add("G", "7");
        map.add("H", "8");
        map.add("I", "9");
        map.add("J", "10"); // reached capacity limit
        map.add("K", "11");

        assertEquals("testGet", "1", map.get("A"));
        assertEquals("testGet", "2", map.get("B"));
        assertEquals("testGet", "3", map.get("C"));
        assertEquals("testGet", "4", map.get("D"));
        assertEquals("testGet", "10", map.get("J"));
        assertEquals("testGet", "11", map.get("K"));
    }

    @Test
    public void remove() throws Exception {
        map = new AssociativeArray<>();

        map.add("A", "1");
        map.add("B", "2");
        map.add("C", "3");
        map.add("D", "4");
        map.add("E", "5");
        map.add("F", "6");
        map.add("G", "7");
        map.add("H", "8");
        map.add("I", "9");
        map.add("J", "10"); // reached capacity limit
        map.add("K", "11");

        assertEquals("testRemove", "1", map.remove("A"));
        assertEquals("testRemove", "2", map.remove("B"));
        assertEquals("testRemove", "3", map.remove("C"));
        assertEquals("testRemove", "4", map.remove("D"));
        assertEquals("testRemove", "10", map.remove("J"));
        assertEquals("testRemove", "11", map.remove("K"));

        assertEquals("testRemove", null, map.get("A"));
        assertEquals("testRemove", null, map.get("B"));
        assertEquals("testRemove", null, map.get("C"));
        assertEquals("testRemove", null, map.get("D"));
        assertEquals("testRemove", null, map.get("J"));
        assertEquals("testRemove", null, map.get("K"));
    }
}