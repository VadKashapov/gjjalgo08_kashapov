package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.Test;

import java.util.HashMap;
import java.util.Random;

import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.kashapovv.Utils.StopWatch.start;

/**
 * Created by Вадим on 17.12.2016.
 */
public class HashMapPerformanceTest {
    private static final int COUNT = 60000;
    private static final int POLYGENE_COUNT = 1000;

    private HashMap hashMap;
    private Random random;
    private int[] randomKeys;
    private String[] words;

    @Test
    public void add() throws Exception {
        hashMap = new HashMap();
        random = new Random(System.currentTimeMillis());
        randomKeys = new int[COUNT];

        for (int i = 0; i < COUNT; i++) {
            randomKeys[i] = random.nextInt(COUNT);
        }

        System.out.println("Random numbers:");

        start();
        for (int i = 0; i < COUNT; i++) {
            hashMap.put(randomKeys[i], random.nextInt(500));
        }
        System.out.println("HashMap.Add(key,value): " + getElapsedTime() + " ms");
    }

    @Test
    public void get() throws Exception {
        hashMap = new HashMap();
        random = new Random(System.currentTimeMillis());
        randomKeys = new int[COUNT];

        for (int i = 0; i < COUNT; i++) {
            randomKeys[i] = random.nextInt(COUNT);
            hashMap.put(randomKeys[i], random.nextInt(500));
        }

        start();
        for (int i = 0; i < COUNT; i++) {
            hashMap.get(randomKeys[i]);
        }
        System.out.println("HashMap.Get(key): " + getElapsedTime() + " ms");
    }

    @Test
    public void remove() throws Exception {
        hashMap = new HashMap();
        random = new Random(System.currentTimeMillis());
        randomKeys = new int[COUNT];

        for (int i = 0; i < COUNT; i++) {
            randomKeys[i] = random.nextInt(COUNT);
            hashMap.put(randomKeys[i], random.nextInt(500));
        }

        start();
        for (int i = 0; i < COUNT; i++) {
            hashMap.remove(randomKeys[i]);
        }
        System.out.println("HashMap.Remove(key): " + getElapsedTime() + " ms");
    }

    private String[] generatePolygenePairs() {
        words = generateWords();
        int squareLength = words.length * words.length;
        String[] pairs = new String[squareLength];
        for (int i = 0, k = squareLength - 1; i < words.length; i++) {
            for (int j = 0; j < words.length; j++) {
                pairs[k--] = words[i] + words[j];
            }
        }
        return pairs;
    }

    private String[] generateWords() {
        words = new String[POLYGENE_COUNT];
        for (char i = 130; i < POLYGENE_COUNT; i++) {
            words[i] = String.valueOf(i);
        }
        return words;
    }

    @Test
    public void add1() throws Exception {
        hashMap = new HashMap();

        System.out.println("Polygene lubricants:");

        String[] pairs = generatePolygenePairs();

        start();
        for (int i = 0; i < pairs.length; i++) {
            hashMap.put(pairs[i], pairs[i]);
        }
        System.out.println("HashMap.Add(key,value): " + getElapsedTime() + " ms");
    }

    @Test
    public void get1() throws Exception {
        hashMap = new HashMap();

        String[] pairs = generatePolygenePairs();

        start();
        for (int i = 0; i < pairs.length; i++) {
            hashMap.get(pairs[i]);
        }
        System.out.println("HashMap.Get(key): " + getElapsedTime() + " ms");
    }

    @Test
    public void remove1() throws Exception {
        hashMap = new HashMap();

        String[] pairs = generatePolygenePairs();

        start();
        for (int i = 0; i < pairs.length; i++) {
            hashMap.remove(pairs[i]);
        }
        System.out.println("HashMap.Remove(key): " + getElapsedTime() + " ms");
    }
}
