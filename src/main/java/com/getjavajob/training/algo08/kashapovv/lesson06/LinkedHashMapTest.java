package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 18.12.2016.
 */
public class LinkedHashMapTest {
    private LinkedHashMap<Character, Character> map;

    @Test
    public void testPut() {
        map = new LinkedHashMap<>();
        assertEquals("testAdd", null, map.put('B', '1'));
        assertEquals("testAdd", '1', (char) map.put('B', 'B'));
        map.put('A', '2');
        assertEquals("testAdd", '2', (char) map.put('A', 'A'));
        map.put('D', '3');
        assertEquals("testAdd", '3', (char) map.put('D', 'D'));
        map.put('C', 'C');
        map.put('E', 'E');
        map.put('F', 'F');

        Collection<Character> result = new ArrayList<>();
        for (Character o : map.keySet()) {
            result.add(o);
        }
        assertEquals("testAdd", "[B, A, D, C, E, F]", Arrays.toString(result.toArray()));
    }
}
