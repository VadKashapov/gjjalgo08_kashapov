package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 15.12.2016.
 */
public class LinkedHashSetTest extends HashMap {
    private LinkedHashSet<Character> set;

    @Test
    public void testAdd() {
        set = new LinkedHashSet<>();
        assertEquals("testAdd", true, set.add('B'));
        assertEquals("testAdd", false, set.add('B'));
        assertEquals("testAdd", true, set.add('A'));
        assertEquals("testAdd", false, set.add('A'));
        set.add('D');
        set.add('C');
        set.add('F');
        set.add('E');

        Collection result = new ArrayList();
        for (Object o : set) {
            result.add(o);
        }
        assertEquals("testAdd", "[B, A, D, C, F, E]", Arrays.toString(result.toArray()));
    }

}
