package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 14.12.2016.
 */
public class MapTest {
    private Map map;

    @Before
    public void setUp() {
        map = new HashMap();
    }

    @After
    public void tearDown() {
        map.clear();
    }


    @Test
    public void size() throws Exception {
        assertEquals("testSize", 0, map.size());
        map.put(1, 1);
        assertEquals("testSize", 1, map.size());
        map.put(2, 1);
        assertEquals("testSize", 2, map.size());
    }

    @Test
    public void isEmpty() throws Exception {
        assertEquals("testIsEmpty", true, map.isEmpty());
        map.put(1, 1);
        assertEquals("testIsEmpty", false, map.isEmpty());
    }

    @Test
    public void containsValue() throws Exception {
        map.put(1, 1);
        map.put(1, 2);
        map.put(2, 1);
        map.put(2, 2);
        map.put(2, 3);
        assertEquals("testContainsValue", false, map.containsValue(1));
        assertEquals("testContainsValue", true, map.containsValue(2));
        assertEquals("testContainsValue", true, map.containsValue(3));
        assertEquals("testContainsValue", false, map.containsValue(4));
    }

    @Test
    public void containsKey() throws Exception {
        map.put(1, 1);
        map.put(1, 2);
        map.put(2, 1);
        map.put(2, 2);
        map.put(2, 3);
        map.put('1', 1);
        map.put('2', 2);
        map.put("1", 1);
        map.put("2", 2);
        assertEquals("testContainsKey", true, map.containsKey(1));
        assertEquals("testContainsKey", true, map.containsKey(2));
        assertEquals("testContainsKey", true, map.containsKey('1'));
        assertEquals("testContainsKey", true, map.containsKey("1"));
        assertEquals("testContainsKey", false, map.containsKey(3));
        assertEquals("testContainsKey", false, map.containsKey('3'));
        assertEquals("testContainsKey", false, map.containsKey("3"));
    }

    @Test
    public void get() throws Exception {
        map.put(1, 1);
        map.put('2', '2');
        map.put("3", "3");
        assertEquals("testGet", "3", map.get("3"));
        assertEquals("testGet", '2', map.get('2'));
        assertEquals("testGet", 1, map.get(1));
    }

    @Test
    public void put() throws Exception {
        assertEquals("testPut", null, map.put(1, 1));
        assertEquals("testPut", 1, map.put(1, 2));
        assertEquals("testPut", 2, map.put(1, 3));
    }

    @Test
    public void remove() throws Exception {
        map.put(1, 1);
        map.put('2', '2');
        map.put("3", "3");

        assertEquals("testRemove", 1, map.remove(1));
        assertEquals("testRemove", '2', map.remove('2'));
        assertEquals("testRemove", "3", map.remove("3"));
    }

    @Test
    public void putAll() throws Exception {
        Map mapToAdd = new HashMap<>();
        mapToAdd.put(1, 1);
        mapToAdd.put(2, 2);
        mapToAdd.put(3, 3);
        map.putAll(mapToAdd);
        assertEquals("testPutAll", 1, map.get(1));
        assertEquals("testPutAll", 2, map.get(2));
        assertEquals("testPutAll", 3, map.get(3));
    }

    @Test
    public void clear() throws Exception {
        Map mapToAdd = new HashMap<>();
        mapToAdd.put(1, 1);
        mapToAdd.put(2, 2);
        mapToAdd.put(3, 3);
        map.putAll(mapToAdd);
        map.clear();
        assertEquals("testClear", true, map.isEmpty());
    }

    @Test
    public void keySet() throws Exception {
        map.put(1, 1);
        map.put('2', '2');
        map.put("3", "3");
        assertEquals("testKeySet", "[3, 2, 1]", Arrays.toString(map.keySet().toArray()));
    }

    @Test
    public void values() throws Exception {
        map.put(1, 4);
        map.put('2', '5');
        map.put("3", "6");
        assertEquals("testValues", "[6, 5, 4]", Arrays.toString(map.values().toArray()));
    }

    @Test
    public void entrySet() throws Exception {
        map.put(1, 4);
        map.put('2', '5');
        map.put("3", "6");
        assertEquals("testEntrySet", "[3=6, 2=5, 1=4]", Arrays.toString(map.entrySet().toArray()));
    }
}
