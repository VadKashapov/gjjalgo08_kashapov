package com.getjavajob.training.algo08.kashapovv.lesson06;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Вадим on 11.12.2016.
 */

interface Matrix<V> {
    V get(int i, int j);

    void set(int i, int j, V value);
}

public class MatrixImpl<V> implements Matrix<V> {
    private Map<Key, V> map;

    public MatrixImpl() {
        map = new HashMap<>();
    }

    public void set(int i, int j, V value) {
        map.put(new Key(i, j), value);
    }

    public V get(int i, int j) {
        return map.get(new Key(i, j));
    }

    private static class Key {
        private int indexI;
        private int indexJ;
        private int hash;

        public Key(int i, int j) {
            this.indexI = i;
            this.indexJ = j;
            hash = 31 * indexI + indexJ;
        }

        @Override
        public boolean equals(Object obj) {
            Key objKey = (Key) obj;
            return indexI == objKey.indexI && indexJ == objKey.indexJ;
        }

        @Override
        public int hashCode() {
            return hash;
        }
    }
}