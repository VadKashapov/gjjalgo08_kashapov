package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 11.12.2016.
 */
public class MatrixImplTest {
    private Matrix<Integer> matrix;

    @Test
    public void set() throws Exception {
        matrix = new MatrixImpl<>();
        matrix.set(10, 10, 11);
        matrix.set(100, 100, 12);
        matrix.set(1000, 1000, 13);
        matrix.set(10000, 10000, 14);

        for (int i = 0; i < 1_000_000; i++) {
            matrix.set(i, 1_000_000, i);
        }
        for (int i = 0; i < 1_000_000; i++) {
            assertEquals(i, (int) matrix.get(i, 1_000_000));
        }
    }

    @Test
    public void get() throws Exception {
        matrix = new MatrixImpl<>();
        matrix.set(10, 10, 11);
        matrix.set(100, 100, 12);
        matrix.set(1000, 1000, 13);
        matrix.set(10000, 10000, 14);
        matrix.set(100000, 100000, 15);

        assertEquals(11, (int) matrix.get(10, 10));
        assertEquals(12, (int) matrix.get(100, 100));
        assertEquals(13, (int) matrix.get(1000, 1000));
        assertEquals(14, (int) matrix.get(10000, 10000));
        assertEquals(15, (int) matrix.get(100000, 100000));
    }
}