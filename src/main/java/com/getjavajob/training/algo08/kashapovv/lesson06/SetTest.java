package com.getjavajob.training.algo08.kashapovv.lesson06;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 14.12.2016.
 */
public class SetTest {
    private Set<Integer> set;

    @Before
    public void setUp() throws Exception {
        set = new HashSet<>();
    }

    @After
    public void tearDown() throws Exception {
        set.clear();
    }

    @Test
    public void add() throws Exception {
        assertEquals("testAdd", false, set.add(1));
        assertEquals("testAdd", false, set.add(1));
        assertEquals("testAdd", true, set.add(2));
        assertEquals("testAdd", false, set.add(2));
        assertEquals("testAdd", true, set.add(3));
        assertEquals("testAdd", false, set.add(3));
        set.add(null);
        set.add(null);
        assertEquals("testAdd", new Integer[]{null, 1, 2, 3}, set.toArray());
    }

    @Test
    public void addAll() throws Exception {
        set.addAll(Arrays.asList(1, 1, 2, 2, 3, 3, null, null));
        assertEquals("testContainsAll", new Integer[]{null, 1, 2, 3}, set.toArray());
    }
}
