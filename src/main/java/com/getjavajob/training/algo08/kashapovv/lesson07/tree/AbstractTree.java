package com.getjavajob.training.algo08.kashapovv.lesson07.tree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    protected Collection<Node<E>> collection;

    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return !isInternal(n);
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Node instance is unsupported");
        }
        return parent(n) == null;
    }

    @Override
    public boolean isEmpty() {
        return (root() == null);
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Collection<Node<E>> preOrder() {
        collection = new ArrayList<>();
        return preOrder(root());
    }

    protected Collection<Node<E>> preOrder(Node<E> node) throws IllegalArgumentException {
        if (node == null) {
            return collection;
        }
        collection.add(node);
        for (Node<E> child : children(node)) {
            preOrder(child);
        }
        return collection;
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() {
        collection = new ArrayList<>();
        return postOrder(root());
    }

    protected Collection<Node<E>> postOrder(Node<E> node) throws IllegalArgumentException {
        if (node == null) {
            return collection;
        }
        for (Node<E> child : children(node)) {
            postOrder(child);
        }
        collection.add(node);
        return collection;
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        collection = new ArrayList<>();
        Queue<Node<E>> queue = new ArrayDeque<>();
        queue.add(root());
        while (!queue.isEmpty()) {
            Node<E> node = queue.remove();
            collection.add(node);
            for (Node<E> child : children(node)) {
                if (child != null) {
                    queue.add(child);
                }
            }
        }
        return collection;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
