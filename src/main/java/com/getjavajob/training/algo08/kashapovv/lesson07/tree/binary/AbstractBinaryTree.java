package com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.AbstractTree;
import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {

    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parent = parent(n);
        if (parent == null) {
            return null;
        }
        return left(parent) == n ? right(parent) : left(parent);
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Unsupported node instance");
        }
        Collection<Node<E>> collection = new ArrayList<>();
        collection.add(left(n));
        collection.add(right(n));
        return collection;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Unsupported node instance");
        }
        int size = 0;
        if (left(n) != null) {
            size++;
        }
        if (right(n) != null) {
            size++;
        }
        return size;
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        collection = new ArrayList<>();
        inOrder(root());
        return collection;
    }

    protected Collection<Node<E>> inOrder(Node<E> node) {
        if (node == null) {
            return collection;
        }
        inOrder(left(node));
        collection.add(node);
        inOrder(right(node));
        return collection;
    }
}
