package com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    private static final int ROOT_INDEX = 0;
    private static final int LEFT_OFFSET = 1;
    private static final int RIGHT_OFFSET = 2;

    private List<Node<E>> nodes;

    public ArrayBinaryTree() {
        nodes = new ArrayList<>();
    }

    private int index(Node<E> p, int offset) {
        if (p == null || !(p instanceof NodeImpl)) {
            throw new IllegalArgumentException("Node instance is unsupported");
        }

        int indexWithoutOffset = nodes.indexOf(p);
        int index = indexWithoutOffset * 2 + offset;
        if (indexWithoutOffset >= 0 && index < nodes.size() * 2 + 1) { // include null children's
            return index;
        } else {
            throw new IllegalArgumentException("Node instance is unsupported");
        }
    }

    private int index(Node<E> p) {
        if (p == null || !(p instanceof NodeImpl)) {
            throw new IllegalArgumentException("Node instance is unsupported");
        }

        int index = nodes.indexOf(p);
        if (index >= 0 && index < nodes.size()) {
            return index;
        } else {
            throw new IllegalArgumentException("No such node in tree");
        }
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int index = index(p, LEFT_OFFSET);
        if (index < nodes.size()) {
            return nodes.get(index);
        } else {
            return null;
        }
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int index = index(p, RIGHT_OFFSET);
        if (index < nodes.size()) {
            return nodes.get(index);
        } else {
            return null;
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        Node<E> newNode = new NodeImpl<>(e);
        int index = index(n, LEFT_OFFSET);
        if (index < nodes.size()) {
            if (nodes.get(index) != null) {
                throw new IllegalArgumentException("Node already has left child");
            } else {
                nodes.set(index, newNode);
            }
        } else {
            while (index > nodes.size()) {
                nodes.add(null);
            }
            nodes.add(index, newNode);
        }
        return newNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        Node<E> newNode = new NodeImpl<>(e);
        int index = index(n, RIGHT_OFFSET);
        if (index < nodes.size()) {
            if (nodes.get(index) != null) {
                throw new IllegalArgumentException("Node already has right child");
            } else {
                nodes.set(index, newNode);
            }
        } else {
            while (index > nodes.size()) {
                nodes.add(null);
            }
            nodes.add(index, newNode);
        }
        return newNode;
    }

    @Override
    public Node<E> root() {
        return nodes.isEmpty() ? null : nodes.get(ROOT_INDEX);
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int index = index(n);
        return index > 0 ? nodes.get((index - 1) / 2) : null;
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (!isEmpty()) {
            throw new IllegalStateException("Root already exist");
        }
        Node<E> root = new NodeImpl<>(e);
        nodes.add(root);
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (left(n) == null) {
            return addLeft(n, e);
        } else if (right(n) == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException("Node already has left and right child");
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        int index = index(n);
        E oldValue = n.getElement();
        nodes.set(index, new NodeImpl<>(e));
        return oldValue;
    }

    @Override
    public E remove(Node<E> node) throws IllegalArgumentException {
        int index = index(node);
        E oldValue = node.getElement();
        switch (childrenNumber(node)) {
            case 2:
                replaceWithSuccessor(node);
                break;
            case 1:
                replaceWithChild(node);
                break;
            case 0:
                nodes.set(index, null);
                break;
            default:
                return null;
        }
        return oldValue;
    }

    private void replaceWithChild(Node<E> node) {
        if (left(node) != null) {
            Node<E> leftNode = left(node);
            set(node, leftNode.getElement());
            if (!reindexChildrenIfNeed(leftNode)) {
                nodes.set(index(leftNode), null);
            }
        } else {
            Node<E> rightNode = right(node);
            set(node, rightNode.getElement());
            if (!reindexChildrenIfNeed(rightNode)) {
                nodes.set(index(rightNode), null);
            }
        }
    }

    private void replaceWithSuccessor(Node<E> node) {
        collection = new ArrayList<>();
        Node<E> mostLeftRightSubtreeNode = inOrder(right(node)).iterator().next();
        set(node, mostLeftRightSubtreeNode.getElement());
        remove(mostLeftRightSubtreeNode);
    }

    private boolean reindexChildrenIfNeed(Node<E> node) {
        if (childrenNumber(node) > 0) {
            for (Node<E> n : preOrder(node)) {
                nodes.set(index(n) / 2 - 1, n);
            }
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        int size = 0;
        for (Node<E> node : nodes) {
            if (node != null) {
                size++;
            }
        }
        return size;
    }

    @Override
    public Collection<Node<E>> nodes() {
        return preOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        E value;

        NodeImpl(E value) {
            this.value = value;
        }

        @Override
        public E getElement() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}
