package com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 11.12.2016.
 */
public class ArrayBinaryTreeTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void leftTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> left123 = arrayBinTree.addLeft(left12, 3);

        assertEquals("testLeft", 2, (int) arrayBinTree.left(arrayBinTree.root()).getElement());
        assertEquals("testLeft", 3, (int) arrayBinTree.left(left12).getElement());

        assertEquals("testLeft", null, arrayBinTree.left(left123));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.left(new ArrayBinaryTree.NodeImpl<Integer>(3));
    }

    @Test
    public void LeftExceptionTest() {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> left123 = arrayBinTree.addLeft(left12, 3);

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.left(null);
    }

    @Test
    public void rightTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> right12 = arrayBinTree.addRight(arrayBinTree.root(), 2);
        arrayBinTree.addRight(right12, 3);

        assertEquals("testRight", 2, (int) arrayBinTree.right(arrayBinTree.root()).getElement());
        assertEquals("testRight", 3, (int) arrayBinTree.right(right12).getElement());

        assertEquals("testRight", null, arrayBinTree.left(right12));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.right(new ArrayBinaryTree.NodeImpl<Integer>(3));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.right(null);
    }

    @Test
    public void addLeftTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(left12, 6); // expand array to create set node case;
        Node<Integer> left123 = arrayBinTree.addLeft(left12, 3);
        Node<Integer> left1234 = arrayBinTree.addLeft(left123, 4);
        Node<Integer> left12345 = arrayBinTree.addLeft(left1234, 5);

        assertEquals("testAddLeft", 2, (int) (left12 = arrayBinTree.left(arrayBinTree.root())).getElement());
        assertEquals("testAddLeft", 3, (int) (left123 = arrayBinTree.left(left12)).getElement());
        assertEquals("testAddLeft", 4, (int) (left1234 = arrayBinTree.left(left123)).getElement());
        assertEquals("testAddLeft", 5, (int) (left12345 = arrayBinTree.left(left1234)).getElement());

        assertEquals("testAddLeft", "[1, 2, 3, 4, 5, 6]",
                Arrays.toString(arrayBinTree.nodes().toArray()));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.addLeft(left12, 6);

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.addLeft(null, 1);
    }

    @Test
    public void addRightTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> right12 = arrayBinTree.addRight(arrayBinTree.root(), 2);
        Node<Integer> left126 = arrayBinTree.addLeft(right12, 6); //
        arrayBinTree.addLeft(left126, 7);                        // expand array to create set node case;
        Node<Integer> right123 = arrayBinTree.addRight(right12, 3);
        Node<Integer> right1234 = arrayBinTree.addRight(right123, 4);

        assertEquals("testAddRight", 2, (int) (right12 = arrayBinTree.right(arrayBinTree.root())).getElement());
        assertEquals("testAddRight", 3, (int) (right123 = arrayBinTree.right(right12)).getElement());
        assertEquals("testAddRight", 4, (int) (right1234 = arrayBinTree.right(right123)).getElement());

        assertEquals("testAddRight", "[1, 2, 6, 7, 3, 4]",
                Arrays.toString(arrayBinTree.nodes().toArray()));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.addRight(right12, 6);

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.addRight(null, 1);
    }

    @Test
    public void addTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        arrayBinTree.add(arrayBinTree.root(), 2);
        arrayBinTree.add(arrayBinTree.root(), 3);

        assertEquals("testAddLeft", "[1, 2, 3]", Arrays.toString(arrayBinTree.nodes().toArray()));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.add(arrayBinTree.root(), 2);
        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.add(null, 2);
    }

    @Test
    public void rootTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();

        assertEquals("testRoot", null, arrayBinTree.root());

        arrayBinTree.addRoot(1);
        assertEquals("testRoot", 1, (int) arrayBinTree.root().getElement());

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.addLeft(null, 1);
    }

    @Test
    public void addRootTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        assertEquals("testAddRoot", 1, (int) arrayBinTree.root().getElement());

        thrown.expect(IllegalStateException.class);
        arrayBinTree.addRoot(5);
    }

    @Test
    public void setTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        assertEquals("testSet", 1, (int) arrayBinTree.set(arrayBinTree.root(), 4));
        assertEquals("testSet", 4, (int) arrayBinTree.root().getElement());

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.set(null, 5);
    }

    @Test
    public void removeTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> right13 = arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testRemove", 2, (int) arrayBinTree.remove(left12));
        assertEquals("testRemove", "[1, 5, 3, 4, 7, 6]", Arrays.toString(arrayBinTree.breadthFirst().toArray()));

        assertEquals("testRemove", 3, (int) arrayBinTree.remove(right13));
        assertEquals("testRemove", "[1, 5, 4, 7, 6]", Arrays.toString(arrayBinTree.breadthFirst().toArray()));

        assertEquals("testRemove", 4, (int) arrayBinTree.remove(left124));
        assertEquals("testRemove", "[1, 5, 6, 7]", Arrays.toString(arrayBinTree.breadthFirst().toArray()));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.remove(null);
    }

    @Test
    public void sizeTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> left123 = arrayBinTree.addLeft(left12, 3);
        Node<Integer> left1234 = arrayBinTree.addLeft(left123, 4);
        arrayBinTree.addLeft(left1234, 5);
        assertEquals("testSize", 5, arrayBinTree.size());
    }

    @Test
    public void nodesTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(left12, 6); // expand array to create set node case;
        Node<Integer> left123 = arrayBinTree.addLeft(left12, 3);
        Node<Integer> left1234 = arrayBinTree.addLeft(left123, 4);
        Node<Integer> left12345 = arrayBinTree.addLeft(left1234, 5);

        assertEquals("testAddLeft", 2, (int) (left12 = arrayBinTree.left(arrayBinTree.root())).getElement());
        assertEquals("testAddLeft", 3, (int) (left123 = arrayBinTree.left(left12)).getElement());
        assertEquals("testAddLeft", 4, (int) (left1234 = arrayBinTree.left(left123)).getElement());
        assertEquals("testAddLeft", 5, (int) (left12345 = arrayBinTree.left(left1234)).getElement());

        assertEquals("testAddLeft", "[1, 2, 3, 4, 5, 6]",
                Arrays.toString(arrayBinTree.nodes().toArray()));
    }

    @Test
    public void preOrderTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testPreOrder", "[1, 2, 4, 6, 5, 7, 3]", Arrays.toString(arrayBinTree.preOrder().toArray()));
    }

    @Test
    public void postOrderTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testPostOrder", "[6, 4, 7, 5, 2, 3, 1]", Arrays.toString(arrayBinTree.postOrder().toArray()));
    }

    @Test
    public void breadthFirstTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testBreadthFirst", "[1, 2, 3, 4, 5, 6, 7]",
                Arrays.toString(arrayBinTree.breadthFirst().toArray()));
    }

    @Test
    public void siblingTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testSibling", right125, arrayBinTree.sibling(left124));
        assertEquals("testSibling", left124, arrayBinTree.sibling(right125));

        assertEquals("testSibling", null, arrayBinTree.sibling(right1246));
        assertEquals("testSibling", null, arrayBinTree.sibling(right1257));
        assertEquals("testSibling", null, arrayBinTree.sibling(arrayBinTree.root()));
    }

    @Test
    public void childrenTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testChildren", "[null, null]", Arrays.toString(arrayBinTree.children(right1246).toArray()));
        assertEquals("testChildren", "[null, 6]", Arrays.toString(arrayBinTree.children(left124).toArray()));
        assertEquals("testChildren", "[4, 5]", Arrays.toString(arrayBinTree.children(left12).toArray()));
        assertEquals("testChildren", "[2, 3]",
                Arrays.toString(arrayBinTree.children(arrayBinTree.root()).toArray()));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.children(null);
    }

    @Test
    public void childrenNumberTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testChildrenNumber", 0, arrayBinTree.childrenNumber(right1257));
        assertEquals("testChildrenNumber", 1, arrayBinTree.childrenNumber(right125));
        assertEquals("testChildrenNumber", 2, arrayBinTree.childrenNumber(left12));
        assertEquals("testChildrenNumber", 2, arrayBinTree.childrenNumber(arrayBinTree.root()));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.childrenNumber(null);
    }

    @Test
    public void inOrderTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testInOrder", "[4, 6, 2, 5, 7, 1, 3]",
                Arrays.toString(arrayBinTree.inOrder().toArray()));
    }

    @Test
    public void isInternalTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> right13 = arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testInternal", true, arrayBinTree.isInternal(arrayBinTree.root()));
        assertEquals("testInternal", true, arrayBinTree.isInternal(left12));
        assertEquals("testInternal", false, arrayBinTree.isInternal(right1246));
        assertEquals("testInternal", false, arrayBinTree.isInternal(right13));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.isInternal(null);
    }

    @Test
    public void isExternalTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> right13 = arrayBinTree.addRight(arrayBinTree.root(), 3);
        Node<Integer> left124 = arrayBinTree.addLeft(left12, 4);
        Node<Integer> right125 = arrayBinTree.addRight(left12, 5);
        Node<Integer> right1246 = arrayBinTree.addRight(left124, 6);
        Node<Integer> right1257 = arrayBinTree.addRight(right125, 7);

        assertEquals("testExternal", false, arrayBinTree.isExternal(arrayBinTree.root()));
        assertEquals("testExternal", false, arrayBinTree.isExternal(left12));
        assertEquals("testExternal", true, arrayBinTree.isExternal(right1246));
        assertEquals("testExternal", true, arrayBinTree.isExternal(right13));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.isInternal(null);
    }

    @Test
    public void isRootTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> left12 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        Node<Integer> right13 = arrayBinTree.addRight(arrayBinTree.root(), 3);

        assertEquals("testIsRoot", true, arrayBinTree.isRoot(arrayBinTree.root()));
        assertEquals("testIsRoot", false, arrayBinTree.isRoot(left12));
        assertEquals("testIsRoot", false, arrayBinTree.isRoot(right13));

        thrown.expect(IllegalArgumentException.class);
        arrayBinTree.isRoot(null);
    }

    @Test
    public void isEmptyTest() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);

        assertEquals("testIsEmpty", false, arrayBinTree.isEmpty());

        arrayBinTree.remove(arrayBinTree.root());
        assertEquals("testIsEmpty", true, arrayBinTree.isEmpty());
    }

    @Test
    public void iterator() throws Exception {
        ArrayBinaryTree<Integer> arrayBinTree = new ArrayBinaryTree<>();
        arrayBinTree.addRoot(1);
        Node<Integer> child1 = arrayBinTree.addLeft(arrayBinTree.root(), 2);
        arrayBinTree.addRight(arrayBinTree.root(), 3);

        Node<Integer> child5 = arrayBinTree.addLeft(child1, 4);
        Node<Integer> child2 = arrayBinTree.addRight(child1, 5);
        Node<Integer> child3 = arrayBinTree.addLeft(child2, 6);
        Node<Integer> child4 = arrayBinTree.addRight(child2, 7);

        Collection<Integer> result = new ArrayList<>();
        Iterator<Integer> iter = arrayBinTree.iterator();
        while (iter.hasNext()) {
            result.add(iter.next());
        }
        assertEquals("testIterator", Arrays.toString(arrayBinTree.nodes().toArray()),
                Arrays.toString(result.toArray()));
    }
}