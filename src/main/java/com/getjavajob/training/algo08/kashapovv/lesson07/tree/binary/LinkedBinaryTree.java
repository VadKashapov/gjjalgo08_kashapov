package com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    protected Node<E> root;

    // nonpublic utility

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        try {
            return (NodeImpl<E>) n;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Unsupported node type");
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root() != null) {
            throw new IllegalStateException("Root already exist");
        }
        return root = createNodeImpl(null, null, e);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (left(n) == null) {
            return addLeft(n, e);
        } else if (right(n) == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException("Node already has left and right child's");
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        if (left(n) != null) {
            throw new IllegalArgumentException("Node already has left child");
        }
        return validate(n).left = createNodeImpl(null, null, e);
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        if (right(n) != null) {
            throw new IllegalArgumentException("Node already has right child");
        }
        return validate(n).right = createNodeImpl(null, null, e);
    }

    protected NodeImpl<E> createNodeImpl(Node<E> left, Node<E> right, E value) {
        return new NodeImpl<>(null, null, value);
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Node instance is unsupported");
        }
        E oldValue = n.getElement();
        validate(n).value = e;
        return oldValue;
    }

    /**
     * Replaces the element at {@link Node} <i>node</i> with <i>e</i>
     *
     * @param node node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> node) throws IllegalArgumentException {
        NodeImpl<E> parent = validate(parent(node));
        NodeImpl<E> nodeImpl = validate(node);
        E oldValue = nodeImpl.getElement();
        switch (childrenNumber(node)) {
            case 2:
                replaceWithSuccessor(nodeImpl);
                break;
            case 1:
                replaceWithChild(parent, nodeImpl);
                break;
            case 0:
                if (isRoot(node)) {
                    root = null;
                } else {
                    removeLeaf(node, parent);
                }
                break;
            default:
                return null;
        }
        return oldValue;
    }

    private void removeLeaf(Node<E> node, NodeImpl<E> parent) {
        if (parent.left == node) {
            parent.left = null;
        } else {
            parent.right = null;
        }
    }

    private void replaceWithChild(NodeImpl<E> parent, NodeImpl<E> nodeImpl) {
        if (parent.left == nodeImpl) {
            parent.left = nodeImpl.left == null ? nodeImpl.right : nodeImpl.left;
        } else {
            parent.right = nodeImpl.left == null ? nodeImpl.right : nodeImpl.left;
        }
        nodeImpl.left = null;
        nodeImpl.right = null;
    }

    protected void replaceWithSuccessor(NodeImpl<E> nodeImpl) {
        collection = new ArrayList<>();
        Node<E> mostLeftRightSubtreeNode = inOrder(right(nodeImpl)).iterator().next();
        nodeImpl.value = mostLeftRightSubtreeNode.getElement();
        remove(mostLeftRightSubtreeNode);
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException("Node instance is unsupported");
        }
        return validate(p).left;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException("Node instance is unsupported");
        }
        return validate(p).right;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(final Node<E> node) throws IllegalArgumentException {
        if (node == null) {
            throw new IllegalArgumentException("Unsupported node instance");
        }
        if (node == root()) {
            return null;
        }
        for (Node<E> iterNode : preOrder()) {
            if (left(iterNode) == node || right(iterNode) == node) {
                return iterNode;
            }
        }
        return null;
    }

    @Override
    public int size() {
        return preOrder().size();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return preOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        Node<E> left;
        Node<E> right;
        E value;

        public NodeImpl(Node<E> left, Node<E> right, E value) {
            this.left = left;
            this.right = right;
            this.value = value;
        }

        @Override
        public E getElement() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        public void setValue(E value) {
            this.value = value;
        }

        public Node<E> getLeft() {
            return left;
        }

        public void setLeft(Node<E> left) {
            this.left = left;
        }

        public Node<E> getRight() {
            return right;
        }

        public void setRight(Node<E> right) {
            this.right = right;
        }
    }
}