package com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 12.12.2016.
 */
public class LinkedBinaryTreeTest extends LinkedBinaryTree {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void validateTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();

        assertEquals("testValidate", null, linkedBinTree.validate(null));

        thrown.expect(IllegalArgumentException.class);
        assertEquals("testValidate", null, linkedBinTree.validate(
                new Node() {
                    @Override
                    public Double getElement() {
                        return null;
                    }
                }
        ));
    }

    @Test
    public void addRootTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        assertEquals("testAddRoot", 1, (int) linkedBinTree.root().getElement());

        thrown.expect(IllegalStateException.class);
        linkedBinTree.addRoot(5);
    }

    @Test
    public void addTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        linkedBinTree.add(linkedBinTree.root(), 2);
        linkedBinTree.add(linkedBinTree.root(), 3);

        assertEquals("testAddLeft", "[1, 2, 3]", Arrays.toString(linkedBinTree.nodes().toArray()));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.add(linkedBinTree.root(), 2);
    }

    @Test
    public void addIllegalArgumetnException() {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        linkedBinTree.add(linkedBinTree.root(), 2);
        linkedBinTree.add(linkedBinTree.root(), 3);

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.add(null, 2);
    }

    @Test
    public void addLeftTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(left12, 6);
        Node<Integer> left123 = linkedBinTree.addLeft(left12, 3);
        Node<Integer> left1234 = linkedBinTree.addLeft(left123, 4);
        Node<Integer> left12345 = linkedBinTree.addLeft(left1234, 5);

        assertEquals("testAddLeft", 2, (int) (left12 = linkedBinTree.left(linkedBinTree.root())).getElement());
        assertEquals("testAddLeft", 3, (int) (left123 = linkedBinTree.left(left12)).getElement());
        assertEquals("testAddLeft", 4, (int) (left1234 = linkedBinTree.left(left123)).getElement());
        assertEquals("testAddLeft", 5, (int) (left12345 = linkedBinTree.left(left1234)).getElement());

        assertEquals("testAddLeft", "[1, 2, 3, 4, 5, 6]", Arrays.toString(linkedBinTree.nodes().toArray()));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.addLeft(left12, 6);
    }

    @Test
    public void addLeftIllegalArgumentException() {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(left12, 6);
        Node<Integer> left123 = linkedBinTree.addLeft(left12, 3);
        Node<Integer> left1234 = linkedBinTree.addLeft(left123, 4);
        Node<Integer> left12345 = linkedBinTree.addLeft(left1234, 5);

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.addLeft(null, 1);
    }

    @Test
    public void addRightTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> right12 = linkedBinTree.addRight(linkedBinTree.root(), 2);
        Node<Integer> left126 = linkedBinTree.addLeft(right12, 6); //
        linkedBinTree.addLeft(left126, 7);
        Node<Integer> right123 = linkedBinTree.addRight(right12, 3);
        Node<Integer> right1234 = linkedBinTree.addRight(right123, 4);

        assertEquals("testAddRight", 2, (int) (right12 = linkedBinTree.right(linkedBinTree.root())).getElement());
        assertEquals("testAddRight", 3, (int) (right123 = linkedBinTree.right(right12)).getElement());
        assertEquals("testAddRight", 4, (int) (right1234 = linkedBinTree.right(right123)).getElement());

        assertEquals("testAddRight", "[1, 2, 6, 7, 3, 4]",
                Arrays.toString(linkedBinTree.nodes().toArray()));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.addRight(right12, 6);
    }

    @Test
    public void addRightExceptionTest() {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> right12 = linkedBinTree.addRight(linkedBinTree.root(), 2);
        Node<Integer> left126 = linkedBinTree.addLeft(right12, 6); //
        linkedBinTree.addLeft(left126, 7);
        Node<Integer> right123 = linkedBinTree.addRight(right12, 3);
        Node<Integer> right1234 = linkedBinTree.addRight(right123, 4);

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.addRight(null, 1);
    }

    @Test
    public void setTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        assertEquals("testSet", 1, (int) linkedBinTree.set(linkedBinTree.root(), 4));
        assertEquals("testSet", 4, (int) linkedBinTree.root().getElement());

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.set(null, 5);
    }

    @Test
    public void removeTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        Node<Integer> right13 = linkedBinTree.addRight(linkedBinTree.root(), 3);
        Node<Integer> left124 = linkedBinTree.addLeft(left12, 4);
        Node<Integer> right125 = linkedBinTree.addRight(left12, 5);
        Node<Integer> right1246 = linkedBinTree.addRight(left124, 6);
        Node<Integer> right1257 = linkedBinTree.addRight(right125, 7);

        assertEquals("testRemove", 2, (int) linkedBinTree.remove(left12));
        assertEquals("testRemove", "[1, 5, 3, 4, 7, 6]", Arrays.toString(linkedBinTree.breadthFirst().toArray()));

        assertEquals("testRemove", 3, (int) linkedBinTree.remove(right13));
        assertEquals("testRemove", "[1, 5, 4, 7, 6]", Arrays.toString(linkedBinTree.breadthFirst().toArray()));

        assertEquals("testRemove", 4, (int) linkedBinTree.remove(left124));
        assertEquals("testRemove", "[1, 5, 6, 7]", Arrays.toString(linkedBinTree.breadthFirst().toArray()));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.remove(null);
    }

    @Test
    public void leftTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        Node<Integer> left123 = linkedBinTree.addLeft(left12, 3);

        assertEquals("testLeft", 2, (int) linkedBinTree.left(linkedBinTree.root()).getElement());
        assertEquals("testLeft", 3, (int) linkedBinTree.left(left12).getElement());

        assertEquals("testLeft", null, linkedBinTree.left(left123));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.left(new ArrayBinaryTree.NodeImpl<Integer>(3));

        leftIllegalArgumentException();
    }

    public void leftIllegalArgumentException() {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        Node<Integer> left123 = linkedBinTree.addLeft(left12, 3);

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.left(null);
    }

    @Test
    public void rightTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> right12 = linkedBinTree.addRight(linkedBinTree.root(), 2);
        linkedBinTree.addRight(right12, 3);

        assertEquals("testRight", 2, (int) linkedBinTree.right(linkedBinTree.root()).getElement());
        assertEquals("testRight", 3, (int) linkedBinTree.right(right12).getElement());

        assertEquals("testRight", null, linkedBinTree.left(right12));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.right(new ArrayBinaryTree.NodeImpl<Integer>(3));
    }

    @Test
    public void rightIllegalArgumentException() {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> right12 = linkedBinTree.addRight(linkedBinTree.root(), 2);
        linkedBinTree.addRight(right12, 3);

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.right(null);
    }

    @Test
    public void rootTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();

        assertEquals("testRoot", null, linkedBinTree.root());

        linkedBinTree.addRoot(1);
        assertEquals("testRoot", 1, (int) linkedBinTree.root().getElement());

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.addLeft(null, 1);
    }

    @Test
    public void parentTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> child1 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(linkedBinTree.root(), 3);

        Node<Integer> child5 = linkedBinTree.addLeft(child1, 4);
        Node<Integer> child2 = linkedBinTree.addRight(child1, 5);
        Node<Integer> child3 = linkedBinTree.addLeft(child2, 6);
        Node<Integer> child4 = linkedBinTree.addRight(child2, 7);

        assertEquals("testParent", 5, (int) linkedBinTree.parent(child3).getElement());
        assertEquals("testParent", 2, (int) linkedBinTree.parent(child5).getElement());
        assertEquals("testParent", 1, (int) linkedBinTree.parent(child1).getElement());
    }

    @Test
    public void sizeTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> child1 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(linkedBinTree.root(), 3);

        Node<Integer> child5 = linkedBinTree.addLeft(child1, 4);
        Node<Integer> child2 = linkedBinTree.addRight(child1, 5);
        Node<Integer> child3 = linkedBinTree.addLeft(child2, 6);
        Node<Integer> child4 = linkedBinTree.addRight(child2, 7);

        assertEquals("testParent", 7, linkedBinTree.size());
    }

    @Test
    public void nodesTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(left12, 6);
        Node<Integer> left123 = linkedBinTree.addLeft(left12, 3);
        Node<Integer> left1234 = linkedBinTree.addLeft(left123, 4);
        Node<Integer> left12345 = linkedBinTree.addLeft(left1234, 5);

        assertEquals("testAddLeft", 2, (int) (left12 = linkedBinTree.left(linkedBinTree.root())).getElement());
        assertEquals("testAddLeft", 3, (int) (left123 = linkedBinTree.left(left12)).getElement());
        assertEquals("testAddLeft", 4, (int) (left1234 = linkedBinTree.left(left123)).getElement());
        assertEquals("testAddLeft", 5, (int) (left12345 = linkedBinTree.left(left1234)).getElement());

        assertEquals("testAddLeft", "[1, 2, 3, 4, 5, 6]",
                Arrays.toString(linkedBinTree.nodes().toArray()));
    }

    @Test
    public void siblingTest() throws Exception {
//            1
//           / \
//          2   3
//         / \
//        4   5
//         \   \
//          6   7

        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(linkedBinTree.root(), 3);
        Node<Integer> left124 = linkedBinTree.addLeft(left12, 4);
        Node<Integer> right125 = linkedBinTree.addRight(left12, 5);
        Node<Integer> right1246 = linkedBinTree.addRight(left124, 6);
        Node<Integer> right1257 = linkedBinTree.addRight(right125, 7);

        assertEquals("testSibling", right125, linkedBinTree.sibling(left124));
        assertEquals("testSibling", left124, linkedBinTree.sibling(right125));

        assertEquals("testSibling", null, linkedBinTree.sibling(right1246));
        assertEquals("testSibling", null, linkedBinTree.sibling(right1257));
        assertEquals("testSibling", null, linkedBinTree.sibling(linkedBinTree.root()));
    }

    @Test
    public void childrenTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        Node<Integer> root = linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        Node<Integer> right13 = linkedBinTree.addRight(linkedBinTree.root(), 3);

        linkedBinTree.addLeft(left12, 4);
        Node<Integer> right125 = linkedBinTree.addRight(left12, 5);
        Node<Integer> left136 = linkedBinTree.addLeft(right13, 6);

        assertEquals("testParent", "[2, 3]", Arrays.toString(linkedBinTree.children(root).toArray()));
        assertEquals("testParent", "[4, 5]", Arrays.toString(linkedBinTree.children(left12).toArray()));
        assertEquals("testParent", "[6, null]", Arrays.toString(linkedBinTree.children(right13).toArray()));
        assertEquals("testParent", "[null, null]", Arrays.toString(linkedBinTree.children(left136).toArray()));
    }

    @Test
    public void childrenNumberTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        Node<Integer> root = linkedBinTree.addRoot(1);
        assertEquals("testParent", 0, linkedBinTree.childrenNumber(root));

        Node<Integer> child1 = linkedBinTree.addLeft(root, 2);
        assertEquals("testParent", 1, linkedBinTree.childrenNumber(root));

        Node<Integer> child2 = linkedBinTree.addRight(root, 3);
        assertEquals("testParent", 2, linkedBinTree.childrenNumber(root));
    }

    @Test
    public void isInternalTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> child1 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(linkedBinTree.root(), 3);

        Node<Integer> child5 = linkedBinTree.addLeft(child1, 4);
        Node<Integer> child2 = linkedBinTree.addRight(child1, 5);
        Node<Integer> child3 = linkedBinTree.addLeft(child2, 6);
        Node<Integer> child4 = linkedBinTree.addRight(child2, 7);

        assertEquals("testParent", true, linkedBinTree.isInternal(child1));
        assertEquals("testParent", true, linkedBinTree.isInternal(child2));
    }

    @Test
    public void isExternalTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> child1 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(linkedBinTree.root(), 3);

        Node<Integer> child5 = linkedBinTree.addLeft(child1, 4);
        Node<Integer> child2 = linkedBinTree.addRight(child1, 5);
        Node<Integer> child3 = linkedBinTree.addLeft(child2, 6);
        Node<Integer> child4 = linkedBinTree.addRight(child2, 7);

        assertEquals("testIsExternal", true, linkedBinTree.isExternal(child3));
        assertEquals("testIsExternal", true, linkedBinTree.isExternal(child4));
        assertEquals("testIsExternal", false, linkedBinTree.isExternal(child2));
    }

    @Test
    public void isRootTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> left12 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        Node<Integer> right13 = linkedBinTree.addRight(linkedBinTree.root(), 3);

        assertEquals("testIsRoot", true, linkedBinTree.isRoot(linkedBinTree.root()));
        assertEquals("testIsRoot", false, linkedBinTree.isRoot(left12));
        assertEquals("testIsRoot", false, linkedBinTree.isRoot(right13));

        thrown.expect(IllegalArgumentException.class);
        linkedBinTree.isRoot(null);
    }

    @Test
    public void isEmptyTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);

        assertEquals("testIsEmpty", false, linkedBinTree.isEmpty());

        linkedBinTree.remove(linkedBinTree.root());
        assertEquals("testIsEmpty", true, linkedBinTree.isEmpty());
    }

    @Test
    public void iteratorTest() throws Exception {
        LinkedBinaryTree<Integer> linkedBinTree = new LinkedBinaryTree<>();
        linkedBinTree.addRoot(1);
        Node<Integer> child1 = linkedBinTree.addLeft(linkedBinTree.root(), 2);
        linkedBinTree.addRight(linkedBinTree.root(), 3);

        Node<Integer> child5 = linkedBinTree.addLeft(child1, 4);
        Node<Integer> child2 = linkedBinTree.addRight(child1, 5);
        Node<Integer> child3 = linkedBinTree.addLeft(child2, 6);
        Node<Integer> child4 = linkedBinTree.addRight(child2, 7);

        Collection<Integer> result = new LinkedList<>();
        Iterator<Integer> iter = linkedBinTree.iterator();
        while (iter.hasNext()) {
            result.add(iter.next());
        }
        assertEquals("testIterator", Arrays.toString(linkedBinTree.nodes().toArray()),
                Arrays.toString(result.toArray()));
    }
}