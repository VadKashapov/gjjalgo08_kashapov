package com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {

    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (val1 == null || val2 == null) {
            throw new IllegalArgumentException("Unsupported compared value");
        }
        return comparator == null ? ((Comparable<? super E>) val1).compareTo(val2) : comparator.compare(val1, val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null || n.getElement() == val) {
            return n;
        }
        if (compare(n.getElement(), val) > 0) {
            return treeSearch(left(n), val);
        } else if (compare(n.getElement(), val) < 0) {
            return treeSearch(right(n), val);
        }
        return n;
    }

    public Node<E> treeSearch(E val) {
        return treeSearch(root(), val);
    }

    public Node<E> insert(E val) {
        Node<E> root = insert(root(), val);
        Node<E> newNode;
        if (root() == null) {
            super.root = root;
            newNode = root;
        } else {
            newNode = treeSearch(root(), val);
        }
        afterElementAdded(newNode);
        return newNode;
    }

    protected Node<E> insert(Node<E> root, E val) {
        if (root == null) {
            root = createNodeImpl(null, null, val);
        } else if (compare(root.getElement(), val) > 0) {
            validate(root).setLeft(insert(left(root), val));
        } else {
            validate(root).setRight(insert(right(root), val));
        }
        return root;
    }

    public E delete(E val) {
        Node<E> nodeToDelete = treeSearch(val);
        Node<E> parent = parent(nodeToDelete);
        E oldValue = remove(nodeToDelete);
        if (parent != null) {
            afterElementRemoved(parent);
        }
        return oldValue;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder = preOrderString(root(), builder.append('('));
        return builder.append(')').toString().replaceAll("\\(n,n\\)", "");
    }

    protected StringBuilder preOrderString(Node<E> node, StringBuilder builder) {
        if (node == null) {
            return builder.append('n');
        }
        builder.append(node.getElement());
        preOrderString(left(node), builder.append('('));
        preOrderString(right(node), builder.append(','));
        return builder.append(')');
    }

    protected void afterElementRemoved(Node<E> parent) {

    }

    protected void afterElementAdded(Node<E> n) {

    }
}
