package com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 15.12.2016.
 */
public class BinarySearchTreeTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    private BinarySearchTree<Integer> sTree;

    @Before
    public void setUp() {
        sTree = new BinarySearchTree<Integer>();
//        sTree = new BinarySearchTree(new Comparator() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                if (o1 == null || o2 == null) {
//                    throw new IllegalArgumentException("Unsupported type");
//                }
//                int i1 = (int) o1;
//                int i2 = (int) o2;
//
//                if (i1 == i2) {
//                    return 0;
//                } else if (i1 > i2) {
//                    return 1;
//                } else {
//                    return -1;
//                }
//            }
//        });

//            7
//           / \
//          4   9
//         / \
//        1   6
//        \   /
//        2   5

        sTree.addRoot(7);
        Node<Integer> left74 = sTree.addLeft(sTree.root(), 4);
        Node<Integer> right79 = sTree.addRight(sTree.root(), 9);
        Node<Integer> left741 = sTree.addLeft(left74, 1);
        Node<Integer> right746 = sTree.addRight(left74, 6);
        Node<Integer> left7465 = sTree.addLeft(right746, 5);
        Node<Integer> right7412 = sTree.addRight(left741, 2);
    }

    @Test
    public void compare() throws Exception {
        assertEquals("testCompare", -1, sTree.compare(5, 6));
        assertEquals("testCompare", 1, sTree.compare(7, 6));
        assertEquals("testCompare", 0, sTree.compare(7, 7));
    }

    @Test
    public void treeSearch() throws Exception {
        assertEquals("testTreeSearch", 5, (int) sTree.treeSearch(sTree.root(), 5).getElement());
        assertEquals("testTreeSearch", 2, (int) sTree.treeSearch(sTree.root(), 2).getElement());
        assertEquals("testTreeSearch", 7, (int) sTree.treeSearch(sTree.root(), 7).getElement());

        assertEquals("testTreeSearch", null, sTree.treeSearch(sTree.root(), 20));
    }

    @Test
    public void toStringTest() throws Exception {
        assertEquals("ToStringTest", "(7(4(1(n,2),6(5,n)),9))", sTree.toString());
    }

    @Test
    public void delete() throws Exception {
        assertEquals("testDelete", 6, (int) sTree.delete(6));
        assertEquals("testDelete", "(7(4(1(n,2),5),9))", sTree.toString());

        assertEquals("testDelete", 7, (int) sTree.delete(7));
        assertEquals("testDelete", "(9(4(1(n,2),5),n))", sTree.toString());

        thrown.expect(IllegalArgumentException.class);
        assertEquals("testDelete", null, sTree.delete(20));

    }

    @Test
    public void insert() throws Exception {
        sTree.insert(3);
        assertEquals("testInsert", "(7(4(1(n,2(n,3)),6(5,n)),9))", sTree.toString());

        thrown.expect(IllegalArgumentException.class);
        sTree.insert(null);
    }
}