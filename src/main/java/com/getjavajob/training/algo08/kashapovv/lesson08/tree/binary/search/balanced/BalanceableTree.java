package com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search.BinarySearchTree;

import java.util.Comparator;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    public BalanceableTree(Comparator<E> comparator) {
        super(comparator);
    }

    public BalanceableTree() {
    }

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        relinkAbove(parent, child);
        if (makeLeftChild) {
            NodeImpl<E> leftParent = validate(left(parent));
            parent.setLeft(child);
            child.setRight(leftParent);
        } else {
            NodeImpl<E> rightParent = validate(right(parent));
            parent.setRight(child);
            child.setLeft(rightParent);
        }
    }

    private void relinkAbove(NodeImpl<E> parent, NodeImpl<E> child) {
        NodeImpl<E> grandparent = validate(parent(child));
        if (grandparent != null) {
            if (left(grandparent) == child) {
                grandparent.setLeft(parent);
            } else {
                grandparent.setRight(parent);
            }
        } else if (isRoot(child)) {
            root = parent;
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        Node parent = parent(n);
        if (left(parent) == n) {
            relink(validate(n), validate(parent), false);
        } else {
            relink(validate(n), validate(parent), true);
        }
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        Node<E> parent = parent(n);
        Node<E> grandparent = parent(parent);

        if (left(grandparent) == parent && left(parent) == n || right(grandparent) == parent && right(parent) == n) {
            rotate(parent);
            return parent;
        } else if (right(grandparent) == parent && left(parent) == n
                || left(grandparent) == parent && right(parent) == n) {
            rotate(n);
            rotate(n);
            return n;
        }
        return null;
    }

    @Override
    protected void afterElementRemoved(Node<E> parent) {
        checkAndBalance(parent);
    }

    @Override
    protected void afterElementAdded(Node<E> newNode) {
        checkAndBalance(newNode);
    }

    protected void checkAndBalance(Node<E> parent) {
        while (parent != null) {
            int factor = balanceFactor(parent);
            if (factor > 1) {
                checkFactorOfRightChild(right(parent));
            } else if (factor < -1) {
                checkFactorOfLeftChild(left(parent));
            }
            parent = parent(parent);
        }
    }

    private void checkFactorOfLeftChild(Node<E> leftOfParent) {
        if (balanceFactor(leftOfParent) > 0) {
            reduceSubtreeHeight(right(leftOfParent));
        } else {
            reduceSubtreeHeight(left(leftOfParent));
        }
    }

    private void checkFactorOfRightChild(Node<E> rightOfParent) {
        if (balanceFactor(rightOfParent) > 0) {
            reduceSubtreeHeight(right(rightOfParent));
        } else {
            reduceSubtreeHeight(left(rightOfParent));
        }
    }

    protected int balanceFactor(Node<E> n) {
        return -height(left(n)) + height(right(n));
    }

    protected int height(Node<E> n) {
        if (n == null) {
            return 0;
        } else {
            return 1 + Math.max(height(left(n)), height(right(n)));
        }
    }
}
