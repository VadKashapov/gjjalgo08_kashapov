package com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search.balanced;

import java.util.Comparator;

/**
 * Created by Вадим on 18.12.2016.
 */
public class BalanceableTreeImpl<E> extends BalanceableTree<E> {

    public BalanceableTreeImpl(Comparator<E> comparator) {
        super(comparator);
    }

    public BalanceableTreeImpl() {
    }
}
