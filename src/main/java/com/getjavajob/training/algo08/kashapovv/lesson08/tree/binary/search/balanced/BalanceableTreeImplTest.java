package com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 18.12.2016.
 */
public class BalanceableTreeImplTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    private BalanceableTreeImpl<Integer> bsTree;

    @Before
    public void setUp() {
        bsTree = new BalanceableTreeImpl(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                int i1 = (int) o1;
                int i2 = (int) o2;

                if (i1 == i2) {
                    return 0;
                } else if (i1 > i2) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

//            7
//           / \
//          4   9
//         / \
//        1   6
//           /
//           5

        bsTree.addRoot(7);
        Node<Integer> left74 = bsTree.addLeft(bsTree.root(), 4);
        Node<Integer> right79 = bsTree.addRight(bsTree.root(), 9);
        Node<Integer> left741 = bsTree.addLeft(left74, 1);
        Node<Integer> right746 = bsTree.addRight(left74, 6);
        Node<Integer> left7465 = bsTree.addLeft(right746, 5);
    }

    @Test
    public void rotate() throws Exception {
        assertEquals("testRotate", "(7(4(1,6(5,n)),9))", bsTree.toString());

        bsTree.rotate(bsTree.treeSearch(bsTree.root(), 4));
        assertEquals("testRotate", "(4(1,7(6(5,n),9)))", bsTree.toString());

        setUp();
        bsTree.rotate(bsTree.treeSearch(bsTree.root(), 6));
        assertEquals("testRotate", "(7(6(4(1,5),n),9))", bsTree.toString());
        bsTree.rotate(bsTree.treeSearch(bsTree.root(), 6));
        assertEquals("testRotate", "(6(4(1,5),7(n,9)))", bsTree.toString());

        setUp();
        thrown.expect(IllegalArgumentException.class);
        bsTree.rotate(bsTree.treeSearch(bsTree.root(), 7));
    }

    @Test
    public void rotateIllegalArgumentException() {
        thrown.expect(IllegalArgumentException.class);
        bsTree.rotate(null);
    }

    @Test
    public void reduceSubtreeHeight() throws Exception {
        bsTree.reduceSubtreeHeight(bsTree.treeSearch(bsTree.root(), 1));
        assertEquals("(4(1,7(6(5,n),9)))", bsTree.toString());

        bsTree.reduceSubtreeHeight(bsTree.treeSearch(bsTree.root(), 6));
        assertEquals("(6(4(1,5),7(n,9)))", bsTree.toString());

        thrown.expect(IllegalArgumentException.class);
        bsTree.reduceSubtreeHeight(null);
    }

    @Test
    public void afterElementRemoved() {
        bsTree.afterElementRemoved(bsTree.treeSearch(bsTree.root(), 6));
        assertEquals("testAfterElementRemoved", "(6(4(1,5),7(n,9)))", bsTree.toString());

        bsTree.afterElementRemoved(null);
    }

    @Test
    public void afterElementAdded() {
        bsTree.afterElementAdded(bsTree.treeSearch(bsTree.root(), 6));
        assertEquals("testAfterElementAdded", "(6(4(1,5),7(n,9)))", bsTree.toString());

        bsTree.afterElementRemoved(null);
    }
}