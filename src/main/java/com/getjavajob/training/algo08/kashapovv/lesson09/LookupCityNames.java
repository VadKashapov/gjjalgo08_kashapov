package com.getjavajob.training.algo08.kashapovv.lesson09;

import java.util.Arrays;
import java.util.Collection;
import java.util.NavigableSet;
import java.util.TreeSet;

import static java.lang.String.CASE_INSENSITIVE_ORDER;

/**
 * Created by Вадим on 21.12.2016.
 */
public class LookupCityNames<E> {
    private NavigableSet<String> cityNames;

    LookupCityNames(Collection<String> cityNames) {
        this.cityNames = new TreeSet<>(CASE_INSENSITIVE_ORDER);
        this.cityNames.addAll(cityNames);
    }

    public static void main(String[] args) {
        Collection<String> cityNames = Arrays.asList("Moscow", "Mogilev", "Murmansk");
        LookupCityNames<String> lookup = new LookupCityNames<>(cityNames);
        System.out.println(Arrays.toString(lookup.lookupFor("mo").toArray()));
    }

    public NavigableSet<String> lookupFor(String keyword) {
        if (keyword == null) {
            return null;
        }
        String keywordLast = keyword + Character.MAX_VALUE;
        return cityNames.subSet(keyword, true, keywordLast, true);
    }
}
