package com.getjavajob.training.algo08.kashapovv.lesson09;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 21.12.2016.
 */
public class LookupCityNamesTest {
    @Test
    public void lookupFor() throws Exception {
        Collection<String> cityNames = Arrays.asList("Moscow", "Mogilev", "Murmansk", "Mosul", "\u0000", "\uFFFF");
        LookupCityNames<String> lookup = new LookupCityNames<>(cityNames);
        assertEquals("[\u0000, Mogilev, Moscow, Mosul, Murmansk, \uFFFF]",
                Arrays.toString(lookup.lookupFor("").toArray()));
        assertEquals("[Mogilev, Moscow, Mosul]", Arrays.toString(lookup.lookupFor("mo").toArray()));
        assertEquals("[\uFFFF]", Arrays.toString(lookup.lookupFor("\uFFFF").toArray()));
        assertEquals("[\u0000]", Arrays.toString(lookup.lookupFor("\u0000").toArray()));
        assertEquals("[Moscow, Mosul]", Arrays.toString(lookup.lookupFor("mos").toArray()));
        assertEquals("[Moscow]", Arrays.toString(lookup.lookupFor("moscow").toArray()));
        assertEquals(null, lookup.lookupFor(null));
    }
}
