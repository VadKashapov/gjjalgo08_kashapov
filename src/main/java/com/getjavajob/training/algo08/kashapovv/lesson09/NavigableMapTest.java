package com.getjavajob.training.algo08.kashapovv.lesson09;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.NavigableMap;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 21.12.2016.
 */
public class NavigableMapTest {
    private NavigableMap<Character, Integer> navigableMap;
    private NavigableMap<Character, Integer> navigableMap1;
    private NavigableMap<Character, Integer> navigableMap2;

    @Before
    public void setUp() throws Exception {
        navigableMap = new TreeMap<>();
        navigableMap.put('B', 2);
        navigableMap.put('A', 1);
        navigableMap.put('D', 4);
        navigableMap.put('C', 3);
        navigableMap.put('F', 6);
        navigableMap.put('E', 5);
    }

    @Test
    public void firstEntry() throws Exception {
        assertEquals("A=1", navigableMap.firstEntry().toString());
    }

    @Test
    public void lastEntry() throws Exception {
        assertEquals("F=6", navigableMap.lastEntry().toString());
    }

    @Test
    public void pollFirstEntry() throws Exception {
        assertEquals("A=1", navigableMap.pollFirstEntry().toString());
        assertEquals(null, navigableMap.get('A'));
    }

    @Test
    public void pollLastEntry() throws Exception {
        assertEquals("F=6", navigableMap.pollLastEntry().toString());
        assertEquals(null, navigableMap.get('F'));
    }

    @Test
    public void lowerEntry() throws Exception {
        assertEquals("F=6", navigableMap.pollLastEntry().toString());
    }

    @Before
    public void setUp1() throws Exception {
        navigableMap1 = new TreeMap<>();
        navigableMap1.put('A', 1);
        navigableMap1.put('D', 4);
        navigableMap1.put('F', 6);
        navigableMap1.put('E', 5);
    }

    @Test
    public void lowerKey() throws Exception {
        assertEquals('A', (char) navigableMap1.lowerKey('B'));
    }

    @Test
    public void floorEntry() throws Exception {
        assertEquals("A=1", navigableMap1.floorEntry('C').toString());
    }

    @Test
    public void floorKey() throws Exception {
        assertEquals("A", navigableMap1.floorKey('C').toString());
    }

    @Test
    public void ceilingEntry() throws Exception {
        assertEquals("D=4", navigableMap1.ceilingEntry('C').toString());
    }

    @Test
    public void ceilingKey() throws Exception {
        assertEquals("D=4", navigableMap1.ceilingEntry('C').toString());
    }

    @Test
    public void higherEntry() throws Exception {
        assertEquals("D=4", navigableMap1.higherEntry('C').toString());
    }

    @Test
    public void higherKey() throws Exception {
        assertEquals("D", navigableMap1.higherKey('C').toString());
    }

    @Before
    public void setUp2() throws Exception {
        navigableMap2 = new TreeMap<>();
        navigableMap2.put('B', 2);
        navigableMap2.put('A', 1);
        navigableMap2.put('D', 4);
        navigableMap2.put('C', 3);
        navigableMap2.put('F', 6);
        navigableMap2.put('E', 5);
    }

    @Test
    public void navigableKeySet() throws Exception {
        assertEquals("[A, B, C, D, E, F]", Arrays.toString(navigableMap2.navigableKeySet().toArray()));
    }

    @Test
    public void descendingKeySet() throws Exception {
        assertEquals("[F, E, D, C, B, A]", Arrays.toString(navigableMap2.descendingKeySet().toArray()));
    }

    @Test
    public void descendingMap() throws Exception {
        assertEquals("[F=6, E=5, D=4, C=3, B=2, A=1]",
                Arrays.toString(navigableMap2.descendingMap().entrySet().toArray()));
    }

    @Test
    public void subMap() throws Exception {
        assertEquals("[B=2, C=3, D=4, E=5]",
                Arrays.toString(navigableMap2.subMap('A', false, 'F', false).entrySet().toArray()));
        assertEquals("[B=2, C=3, D=4, E=5, F=6]",
                Arrays.toString(navigableMap2.subMap('A', false, 'F', true).entrySet().toArray()));
        assertEquals("[A=1, B=2, C=3, D=4, E=5]",
                Arrays.toString(navigableMap2.subMap('A', true, 'F', false).entrySet().toArray()));
        assertEquals("[A=1, B=2, C=3, D=4, E=5, F=6]",
                Arrays.toString(navigableMap2.subMap('A', true, 'F', true).entrySet().toArray()));
    }

    @Test
    public void headMap() throws Exception {
        assertEquals("[F=6, E=5, D=4, C=3, B=2, A=1]",
                Arrays.toString(navigableMap2.descendingMap().entrySet().toArray()));
    }

    @Test
    public void tailMap() throws Exception {
        assertEquals("[F=6, E=5, D=4, C=3, B=2, A=1]",
                Arrays.toString(navigableMap2.descendingMap().entrySet().toArray()));
    }
}
