package com.getjavajob.training.algo08.kashapovv.lesson09;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 19.12.2016.
 */
public class NavigableSetTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    public NavigableSet<Character> navigableSet;

    @Before
    public void setUp() throws Exception {
        navigableSet = new TreeSet<>();
        navigableSet.add('B');
        navigableSet.add('A');
        navigableSet.add('D');
        navigableSet.add('C');
        navigableSet.add('F');
        navigableSet.add('E');
    }

    @Test
    public void iterator() throws Exception {
        Iterator<Character> iter = navigableSet.iterator();
        ArrayList<Character> list = new ArrayList<Character>();
        while (iter.hasNext()) {
            list.add(iter.next());
        }
        assertEquals("[A, B, C, D, E, F]", Arrays.toString(list.toArray()));
    }

    @Test
    public void descendingIterator() throws Exception {
        Iterator<Character> iter = navigableSet.descendingIterator();
        ArrayList<Character> list = new ArrayList<Character>();
        while (iter.hasNext()) {
            list.add(iter.next());
        }
        assertEquals("[F, E, D, C, B, A]", Arrays.toString(list.toArray()));
    }

    @Test
    public void descendingSet() throws Exception {
        NavigableSet desc = navigableSet.descendingSet();
        assertEquals("[F, E, D, C, B, A]", Arrays.toString(desc.toArray()));
    }

    @Test
    public void subSet() throws Exception {
        assertEquals("[B, C, D, E]", Arrays.toString(navigableSet.subSet('A', false, 'F', false).toArray()));
        assertEquals("[B, C, D, E, F]", Arrays.toString(navigableSet.subSet('A', false, 'F', true).toArray()));
        assertEquals("[A, B, C, D, E]", Arrays.toString(navigableSet.subSet('A', true, 'F', false).toArray()));
        assertEquals("[A, B, C, D, E, F]", Arrays.toString(navigableSet.subSet('A', true, 'F', true).toArray()));
    }

    @Test
    public void subSetIllegalArgumentException() {
        thrown.expect(IllegalArgumentException.class);
        navigableSet.subSet('F', false, 'B', false);
    }

    @Test
    public void subSetNullPointerException() {
        NavigableSet<Number> navigableSet = new TreeSet<>();
        thrown.expect(NullPointerException.class);
        navigableSet.subSet(1, false, null, false);
    }

    @Test
    public void subSetClassCastException() {
        NavigableSet<Number> navigableSet = new TreeSet<>();
        thrown.expect(ClassCastException.class);
        navigableSet.subSet(1, false, 2.2, false);
    }

    @Test
    public void headSet() throws Exception {
        assertEquals("[A, B]", Arrays.toString(navigableSet.headSet('C', false).toArray()));
        assertEquals("[A, B, C]", Arrays.toString(navigableSet.headSet('C', true).toArray()));
    }

    @Test
    public void tailSet() throws Exception {
        assertEquals("[D, E, F]", Arrays.toString(navigableSet.tailSet('C', false).toArray()));
        assertEquals("[C, D, E, F]", Arrays.toString(navigableSet.tailSet('C', true).toArray()));
    }

    @Test
    public void lower() throws Exception {
        assertEquals('B', (char) navigableSet.lower('C'));
        assertEquals('A', (char) navigableSet.lower('B'));
    }

    @Test
    public void floor() throws Exception {
        navigableSet = new TreeSet<>();
        navigableSet.add('A');
        navigableSet.add('D');
        navigableSet.add('C');
        navigableSet.add('F');

        assertEquals('A', (char) navigableSet.floor('B'));
        assertEquals('D', (char) navigableSet.floor('E'));
    }

    @Test
    public void ceiling() throws Exception {
        navigableSet = new TreeSet<>();
        navigableSet.add('A');
        navigableSet.add('D');
        navigableSet.add('C');
        navigableSet.add('F');

        assertEquals('C', (char) navigableSet.ceiling('B'));
        assertEquals('F', (char) navigableSet.ceiling('E'));
    }

    @Test
    public void higher() throws Exception {
        assertEquals('D', (char) navigableSet.higher('C'));
        assertEquals('C', (char) navigableSet.higher('B'));
    }

    @Test
    public void pollFirst() throws Exception {
        assertEquals('A', (char) navigableSet.pollFirst());
    }

    @Test
    public void pollLast() throws Exception {
        assertEquals('F', (char) navigableSet.pollLast());
    }

}
