package com.getjavajob.training.algo08.kashapovv.lesson09;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 21.12.2016.
 */
public class SortedMapTest {
    private SortedMap<Character, Integer> sortMap;

    @Before
    public void setUp() throws Exception {
        sortMap = new TreeMap<>();
        sortMap.put('B', 2);
        sortMap.put('A', 1);
        sortMap.put('D', 4);
        sortMap.put('C', 3);
        sortMap.put('F', 6);
        sortMap.put('E', 5);
    }

    @Test
    public void comparator() throws Exception {
        SortedMap<String, String> sortMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.contains(o2)) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        Comparator<? super String> comp = sortMap.comparator();
        assertEquals(1, comp.compare("mogilev", "mo"));
    }

    @Test
    public void firstKey() throws Exception {
        assertEquals('A', (char) sortMap.firstKey());
    }

    @Test
    public void lastKey() throws Exception {
        assertEquals('F', (char) sortMap.lastKey());
    }

    @Test
    public void values() throws Exception {
        assertEquals("[1, 2, 3, 4, 5, 6]", Arrays.toString(sortMap.values().toArray()));
    }

    @Test
    public void entrySet() throws Exception {
        assertEquals("[A=1, B=2, C=3, D=4, E=5, F=6]", Arrays.toString(sortMap.entrySet().toArray()));
    }

    @Test
    public void subMap() throws Exception {
        assertEquals("[A=1, B=2, C=3, D=4]", Arrays.toString(sortMap.subMap('A', 'E').entrySet().toArray()));
    }

    @Test
    public void headMap() throws Exception {
        assertEquals("[A=1, B=2]", Arrays.toString(sortMap.headMap('C').entrySet().toArray()));
    }

    @Test
    public void tailMap() throws Exception {
        assertEquals("[C=3, D=4, E=5, F=6]", Arrays.toString(sortMap.tailMap('C').entrySet().toArray()));
    }

    @Test
    public void keySet() throws Exception {
        assertEquals("[A, B, C, D, E, F]", Arrays.toString(sortMap.keySet().toArray()));
    }

}
