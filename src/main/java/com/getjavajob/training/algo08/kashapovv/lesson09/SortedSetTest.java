package com.getjavajob.training.algo08.kashapovv.lesson09;

import com.getjavajob.training.algo08.kashapovv.Utils.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 19.12.2016.
 */

public class SortedSetTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    private SortedSet<Number> sortSet;

    @Before
    public void setUp() throws Exception {
        sortSet = new TreeSet<>();
        sortSet.add(1);
        sortSet.add(2);
        sortSet.add(3);
        sortSet.add(4);
        sortSet.add(5);
        sortSet.add(6);
        sortSet.add(7);
    }

    @Test
    public void subSet() throws Exception {
        assertEquals("testSubSet", new Integer[]{3, 4, 5}, sortSet.subSet(3, 6).toArray());

        thrown.expect(NullPointerException.class);
        assertEquals("testSubSet", new Integer[]{3, 4, 5}, sortSet.subSet(null, null).toArray());
    }

    @Test
    public void subSetException1() {
        thrown.expect(IllegalArgumentException.class);
        assertEquals("testSubSet", new Integer[]{3, 4, 5}, sortSet.subSet(7, 4).toArray());
    }

    @Test
    public void subSetException2() {
        thrown.expect(ClassCastException.class);
        assertEquals("testSubSet", new Integer[]{3, 4, 5}, sortSet.subSet(3, 5.4).toArray());
    }

    @Test
    public void headSet() throws Exception {
        Assert.assertEquals("testHeadSet", new Integer[]{1, 2, 3, 4}, sortSet.headSet(5).toArray());
        Assert.assertEquals("testHeadSet", new Integer[]{1, 2, 3, 4, 5, 6, 7}, sortSet.headSet(10).toArray());
    }

    @Test
    public void headSetException2() {
        thrown.expect(NullPointerException.class);
        assertEquals("testHeadSet", new Integer[]{1, 2, 3, 4}, sortSet.headSet(null).toArray());
    }

    @Test
    public void headSetException1() {
        thrown.expect(ClassCastException.class);
        assertEquals("testHeadSet", new Integer[]{1, 2, 3, 4}, sortSet.headSet(5.4).toArray());
    }

    @Test
    public void tailSet() throws Exception {
        Assert.assertEquals("testTailSet", new Integer[]{3, 4, 5, 6, 7}, sortSet.tailSet(3).toArray());
        Assert.assertEquals("testTailSet", new Integer[]{}, sortSet.tailSet(10).toArray());
    }

    @Test
    public void tailSetException2() {
        thrown.expect(NullPointerException.class);
        assertEquals("testTailSet", new Integer[]{3, 4, 5, 6, 7}, sortSet.tailSet(null).toArray());
    }

    @Test
    public void tailSetException1() {
        thrown.expect(ClassCastException.class);
        assertEquals("testTailSet", new Integer[]{3, 4, 5, 6, 7}, sortSet.tailSet(3.4).toArray());
    }

    @Test
    public void comparator() throws Exception {

        SortedSet<Number> sortSet = new TreeSet<>(new Comparator<Number>() {
            @Override
            public int compare(Number o1, Number o2) {
                int i1 = (int) o1;
                int i2 = (int) o2;
                if (i1 > i2) {
                    return 1;
                } else if (i1 == i2) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });

        Comparator<? super Number> comp = sortSet.comparator();

        assertEquals(-1, comp.compare(1, 2));
        assertEquals(1, comp.compare(2, 1));
        assertEquals(0, comp.compare(1, 1));

        thrown.expect(ClassCastException.class);
        assertEquals(0, comp.compare(1, 1.2));
    }

    @Test
    public void compException1() {
        Comparator<? super Number> comp = sortSet.comparator();
        thrown.expect(NullPointerException.class);
        assertEquals(0, comp.compare(1, null));
    }

    @Test
    public void first() throws Exception {
        assertEquals(1, (int) sortSet.first());

        SortedSet<Number> sortSet = new TreeSet<>();
        thrown.expect(NoSuchElementException.class);
        assertEquals(1, (int) sortSet.first());
    }

    @Test
    public void last() throws Exception {
        assertEquals(7, (int) sortSet.last());

        SortedSet<Number> sortSet = new TreeSet<>();
        thrown.expect(NoSuchElementException.class);
        assertEquals(1, (int) sortSet.last());
    }

}
