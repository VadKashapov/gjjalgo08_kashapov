package com.getjavajob.training.algo08.kashapovv.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import com.getjavajob.training.algo08.kashapovv.lesson07.tree.binary.LinkedBinaryTree;
import com.getjavajob.training.algo08.kashapovv.lesson08.tree.binary.search.balanced.BalanceableTree;

import java.util.ArrayList;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E> extends BalanceableTree<E> {

    @Override
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        try {
            return (NodeImpl<E>) n;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Unsupported node type");
        }
    }

    @Override
    protected StringBuilder preOrderString(Node<E> node, StringBuilder builder) {
        if (node == null) {
            return builder.append('n');
        }
        builder.append(node.getElement());
        if (validate(node).getColor() != null) {
            builder.append(validate(node).getColor().name().charAt(0));
        }
        preOrderString(left(node), builder.append('('));
        preOrderString(right(node), builder.append(','));
        return builder.append(')');
    }

    @Override
    protected NodeImpl<E> createNodeImpl(Node<E> left, Node<E> right, E value) {
        return new NodeImpl<>(null, null, value, null);
    }

    private boolean isBlack(Node<E> n) {
        return n == null || validate(n).getColor() == NodeImpl.Color.BLACK;
    }

    private boolean isRed(Node<E> n) {
        return n != null && validate(n).getColor() == NodeImpl.Color.RED;
    }

    private void makeBlack(Node<E> n) {
        if (n == null) {
            throw new IllegalArgumentException("Cannot change NIL-leaf color");
        }
        validate(n).setColor(NodeImpl.Color.BLACK);
    }

    private void makeRed(Node<E> n) {
        if (n == null) {
            throw new IllegalArgumentException("Cannot change NIL-leaf color");
        }
        validate(n).setColor(NodeImpl.Color.RED);
    }

    @Override
    protected void afterElementAdded(Node<E> newNode) {
        makeRed(newNode);
        insertCase1(newNode);
    }

    private void insertCase1(Node<E> newNode) {
        if (isRoot(newNode)) {
            makeBlack(newNode);
        } else {
            insertCase2(newNode);
        }
    }

    private void insertCase2(Node<E> node) {
        if (isBlack(parent(node))) {
            return;
        } else {
            insertCase3(node);
        }
    }

    private void insertCase3(Node<E> node) {
        Node<E> parent = parent(node);
        Node<E> uncle = sibling(parent);
        Node<E> grandparent = parent(parent);
        if (uncle != null && isRed(uncle) && isRed(parent)) {
            makeBlack(parent);
            makeBlack(uncle);
            makeRed(grandparent);
            afterElementAdded(grandparent);
        } else {
            insertCase4(node, parent, grandparent);
        }
    }

    private void insertCase4(Node<E> node, Node<E> parent, Node<E> grandparent) {
        if ((node == right(parent)) && (parent == left(grandparent))) {
            rotate(node);
            node = left(node);
        } else if ((node == left(parent)) && parent == right(grandparent)) {
            rotate(node);
            node = right(node);
        }
        insertCase5(node);
    }

    private void insertCase5(Node<E> node) {
        Node<E> parent = parent(node);
        Node<E> grandparent = parent(parent);

        makeBlack(parent);
        makeRed(grandparent);
        rotate(parent);
    }

    @Override
    protected void replaceWithSuccessor(LinkedBinaryTree.NodeImpl<E> nodeImpl) {
        replaceWithPredecessor(nodeImpl);
    }

    protected void replaceWithPredecessor(LinkedBinaryTree.NodeImpl<E> nodeImpl) {
        collection = new ArrayList<>();
        Node<E> mostLeftRightSubtreeNode = null;
        for (Node<E> n : inOrder(left(nodeImpl))) {
            mostLeftRightSubtreeNode = n;
        }
        nodeImpl.setValue(mostLeftRightSubtreeNode.getElement());
        afterElementRemoved(mostLeftRightSubtreeNode); // calling before remove cause
        remove(mostLeftRightSubtreeNode);              // nil-leaf not represented as objects
    }

    @Override
    public E delete(E val) {
        Node<E> nodeToDelete = treeSearch(root(), val);
        if (nodeToDelete == null) {
            throw new IllegalArgumentException("No such node in tree to delete");
        }
        if (childrenNumber(nodeToDelete) > 1) {
            E oldValue = remove(nodeToDelete);
            return oldValue;
        } else {
            afterElementRemoved(nodeToDelete); // calling before remove cause nil-leaf not represented as objects
            E oldValue = remove(nodeToDelete);
            return oldValue;
        }
    }

    public Node<E> addRoot(E val, NodeImpl.Color color) {
        NodeImpl<E> n = validate(addRoot(val));
        n.setColor(color);
        return n;
    }

    public Node<E> addLeft(Node<E> node, E val, NodeImpl.Color color) {
        NodeImpl<E> n = validate(addLeft(node, val));
        n.setColor(color);
        return n;
    }

    public Node<E> addRight(Node<E> node, E val, NodeImpl.Color color) {
        NodeImpl<E> n = validate(addRight(node, val));
        n.setColor(color);
        return n;
    }

    @Override
    protected void afterElementRemoved(Node<E> node) {
        Node<E> child = left(node) == null ? right(node) : left(node);
        if (isBlack(node)) {
            if (child != null && isRed(child)) {
                makeBlack(child);
            } else {
                deleteCase1(node);
            }
        }
    }

    private void deleteCase1(Node<E> node) {
        if (parent(node) != null) {
            deleteCase2(node);
        }
    }

    private void deleteCase2(Node<E> node) {
        Node<E> sibling = sibling(node);
        Node<E> parent = parent(node);

        if (isRed(sibling)) {
            makeRed(parent);
            makeBlack(sibling);
            rotate(sibling);
        }
        deleteCase3(node);
    }

    private void deleteCase3(Node<E> node) {
        Node<E> sibling = sibling(node);
        Node<E> parent = parent(node);

        if (sibling != null) {
            if (isBlack(parent) && isBlack(sibling) && isBlack(left(sibling)) && isBlack(right(sibling))) {
                makeRed(sibling);
                deleteCase1(parent);
            } else {
                deleteCase4(node);
            }
        }
    }

    private void deleteCase4(Node<E> node) {
        Node<E> sibling = sibling(node);
        Node<E> parent = parent(node);

        if (sibling != null) {
            if (isRed(parent) && isBlack(sibling) && isBlack(left(sibling)) && isBlack(right(sibling))) {
                makeRed(sibling);
                makeBlack(parent);
            } else {
                deleteCase5(node);
            }
        }
    }

    private void deleteCase5(Node<E> node) {
        Node<E> sibling = sibling(node);
        Node<E> parent = parent(node);
        if (isBlack(sibling)) {
            if (left(parent) == node && isBlack(right(sibling)) && isRed(left(sibling))) {
                makeRed(sibling);
                makeBlack(left(sibling));
                rotate(left(sibling));
            } else if (right(parent) == node && isBlack(left(sibling)) && isRed(right(sibling))) {
                makeRed(sibling);
                makeBlack(right(sibling));
                rotate(right(sibling));
            }
        }
        deleteCase6(node);
    }

    private void deleteCase6(Node<E> node) {
        Node<E> sibling = sibling(node);
        Node<E> parent = parent(node);

        validate(sibling).setColor(validate(parent).getColor());
        makeBlack(parent);

        if (node == left(parent)) {
            makeBlack(right(sibling));
            rotate(sibling);
        } else {
            makeBlack(left(sibling));
            rotate(sibling);
        }
    }

    protected static class NodeImpl<E> extends LinkedBinaryTree.NodeImpl<E> {
        Color color;

        public NodeImpl(Node<E> left, Node<E> right, E value, Color color) {
            super(left, right, value);
            this.color = color;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        public static enum Color {BLACK, RED}
    }
}