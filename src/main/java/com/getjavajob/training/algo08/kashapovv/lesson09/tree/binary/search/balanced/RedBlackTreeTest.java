package com.getjavajob.training.algo08.kashapovv.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo08.kashapovv.lesson07.tree.Node;
import org.junit.Before;
import org.junit.Test;

import static com.getjavajob.training.algo08.kashapovv.lesson09.tree.binary.search.balanced.RedBlackTree.NodeImpl.Color.BLACK;
import static com.getjavajob.training.algo08.kashapovv.lesson09.tree.binary.search.balanced.RedBlackTree.NodeImpl.Color.RED;
import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 21.12.2016.
 */
public class RedBlackTreeTest {
    private RedBlackTree<Integer> rbTree;

    @Before
    public void setUp() {
        rbTree = new RedBlackTree<Integer>();
    }

    @Test
    public void afterElementAdded() throws Exception {
        rbTree.insert(7);
        rbTree.insert(4);
        rbTree.insert(9);
        rbTree.insert(10);
        rbTree.insert(13);
        rbTree.insert(2);
        rbTree.insert(1);
        rbTree.insert(6);
        rbTree.insert(5);
//             7B
//            /  \
//         2R     10B
//        /  \    /  \
//      1B   5B  9R  13R
//           / \
//         4R  6R
        assertEquals("(7B(2R(1B,5B(4R,6R)),10B(9R,13R)))", rbTree.toString());
        rbTree.insert(3);
//             5B
//            /  \
//         2R     7R
//        /  \    /  \
//      1B   4B  6B  10B
//           /      /   \
//         3R      9R   13R
        assertEquals("(5B(2R(1B,4B(3R,n)),7R(6B,10B(9R,13R))))", rbTree.toString());
    }

    @Test
    public void afterElementAddedCase1() throws Exception {
        rbTree.insert(7);
        assertEquals("(7B)", rbTree.toString());
        rbTree.insert(8);
        assertEquals("(7B(n,8R))", rbTree.toString());
    }

    // case 2 trivial;

    @Test
    public void afterElementAddedCase3_1() throws Exception {
//            7B               7R               7B
//            / \              / \              / \
//          4R   10R   ->    4B   10B  ->     4B   10B
//          /                /                /
//        3R               3R               3R
        rbTree.addRoot(7, BLACK);
        Node<Integer> left7_4 = rbTree.addLeft(rbTree.root(), 4, RED);
        Node<Integer> right7_10 = rbTree.addRight(rbTree.root(), 10, RED);
        Node<Integer> left7_4_3 = rbTree.addLeft(left7_4, 3, RED);
        rbTree.afterElementAdded(rbTree.treeSearch(3));
        assertEquals("(7B(4B(3R,n),10B))", rbTree.toString());
    }

    @Test
    public void afterElementAddedCase4_5() throws Exception {
//            7B             7B            5B
//            / \            / \           / \
//          4R   10B   ->  5R   10R ->   4R   7R
//            \            /                    \
//            5R         4R                      10B
        rbTree.addRoot(7, BLACK);
        Node<Integer> left7_4 = rbTree.addLeft(rbTree.root(), 4, RED);
        Node<Integer> right7_10 = rbTree.addRight(rbTree.root(), 10, BLACK);
        Node<Integer> right7_4_5 = rbTree.addRight(left7_4, 5, RED);
        rbTree.afterElementAdded(rbTree.treeSearch(5));
        assertEquals("(5B(4R,7R(n,10B)))", rbTree.toString());
    }

    @Test
    public void afterElementRemoved() throws Exception {
        rbTree.insert(30);
        rbTree.insert(15);
        rbTree.insert(50);
        rbTree.insert(10);
        rbTree.insert(20);
        rbTree.insert(18);
        rbTree.insert(21);
        rbTree.insert(60);
//             30B
//            /    \
//         15R      50B
//        /  \        \
//      10B   20B     60R
//             / \
//           18R 21R
        assertEquals("(30B(15R(10B,20B(18R,21R)),50B(n,60R)))", rbTree.toString());
        rbTree.delete(18);
//             30B
//            /    \
//         15R      50B
//        /  \        \
//      10B   20B     60R
//             \
//             21R
        assertEquals("(30B(15R(10B,20B(n,21R)),50B(n,60R)))", rbTree.toString());
        rbTree.delete(10);
//             30B
//            /    \
//         20R      50B
//        /  \         \
//      15B   21B      60R
        assertEquals("(30B(20R(15B,21B),50B(n,60R)))", rbTree.toString());
        rbTree.delete(15);
//             30B
//            /    \
//         20B      50B
//           \         \
//           21R        60R
        assertEquals("(30B(20B(n,21R),50B(n,60R)))", rbTree.toString());
    }

    // case1 trivial;

    @Test
    public void afterElementRemovedCase2_4() throws Exception {
//            7B                10B           10B
//            / \               / \          /  \
//          4B   10R   ->      7R  13B  ->  7B   13B
//               / \           / \         /  \
//              9B  13B      4B  9B       4B  9R

        rbTree.addRoot(7, BLACK);
        Node<Integer> left7_4 = rbTree.addLeft(rbTree.root(), 4, BLACK);
        Node<Integer> right7_10 = rbTree.addRight(rbTree.root(), 10, RED);
        Node<Integer> left7_10_9 = rbTree.addLeft(right7_10, 9, BLACK);
        Node<Integer> right7_10_13 = rbTree.addRight(right7_10, 13, BLACK);
        assertEquals("(7B(4B,10R(9B,13B)))", rbTree.toString());
        rbTree.afterElementRemoved(left7_4);
        assertEquals("(10B(7B(4B,9R),13B))", rbTree.toString());
    }

    @Test
    public void afterElementRemovedCase3_1() throws Exception {
//            7B              7B
//            / \             / \
//          4B   10B   ->   4B   10R
//               / \             / \
//              9B  13B         9B  13B
        rbTree.addRoot(7, BLACK);
        Node<Integer> left4 = rbTree.addLeft(rbTree.root(), 4, BLACK);
        Node<Integer> right10 = rbTree.addRight(rbTree.root(), 10, BLACK);
        Node<Integer> left10_9 = rbTree.addLeft(right10, 9, BLACK);
        Node<Integer> right710_13 = rbTree.addRight(right10, 13, BLACK);
        assertEquals("(7B(4B,10B(9B,13B)))", rbTree.toString());
        rbTree.afterElementRemoved(left4);
        assertEquals("(7B(4B,10R(9B,13B)))", rbTree.toString());

    }

    @Test
    public void afterElementRemovedCase4() throws Exception {
//            7R              7B
//            / \             / \
//          4B   10B   ->   4B   10R
//               / \             / \
//              9B  13B         9B  13B
        rbTree.addRoot(7, RED);
        Node<Integer> left4 = rbTree.addLeft(rbTree.root(), 4, BLACK);
        Node<Integer> right10 = rbTree.addRight(rbTree.root(), 10, BLACK);
        Node<Integer> left10_9 = rbTree.addLeft(right10, 9, BLACK);
        Node<Integer> right7_10_13 = rbTree.addRight(right10, 13, BLACK);
        assertEquals("(7R(4B,10B(9B,13B)))", rbTree.toString());
        rbTree.afterElementRemoved(left4);
        assertEquals("(7B(4B,10R(9B,13B)))", rbTree.toString());
    }

    @Test
    public void afterElementRemovedCase5_6() throws Exception {
//            7B              7B                   9B
//            / \             / \                 /  \
//          4B   10B   ->   4B   9B       ->    7B   10B
//               / \               \            /       \
//              9R  13B            10R         4B       13B
//                                    \
//                                    13B
        rbTree.addRoot(7, BLACK);
        Node<Integer> left4 = rbTree.addLeft(rbTree.root(), 4, BLACK);
        Node<Integer> right10 = rbTree.addRight(rbTree.root(), 10, BLACK);
        Node<Integer> left10_9 = rbTree.addLeft(right10, 9, RED);
        Node<Integer> right7_10_13 = rbTree.addRight(right10, 13, BLACK);
        assertEquals("(7B(4B,10B(9R,13B)))", rbTree.toString());
        rbTree.afterElementRemoved(left4);
        assertEquals("(9B(7B(4B,n),10B(n,13B)))", rbTree.toString());
    }
}