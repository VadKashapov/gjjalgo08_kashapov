package com.getjavajob.training.algo08.kashapovv.lesson10;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 23.12.2016.
 */
public class ArraysTest {

    @Test
    public void sort() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers);
        assertEquals(new Integer[]{0, 1, 2, 3, 4, 5, 5, 7, 9}, numbers);
    }

    @Test
    public void sort1() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers, Collections.reverseOrder());
        assertEquals(new Integer[]{9, 7, 5, 5, 4, 3, 2, 1, 0}, numbers);
    }

    @Test
    public void sort2() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers, 3, numbers.length);
        assertEquals(new Integer[]{5, 5, 1, 0, 2, 3, 4, 7, 9}, numbers);
    }

    @Test
    public void sort3() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers, 3, numbers.length, null);
        assertEquals(new Integer[]{5, 5, 1, 0, 2, 3, 4, 7, 9}, numbers);
    }

    @Test
    public void binarySearch() throws Exception {
        int[] numbers = new int[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers);
        assertEquals(3, Arrays.binarySearch(numbers, 3));
    }

    @Test
    public void binarySearch1() throws Exception {
        int[] numbers = new int[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers);
        assertEquals(3, Arrays.binarySearch(numbers, 3, numbers.length, 3));
    }

    @Test
    public void binarySearch2() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers);
        assertEquals(3, Arrays.binarySearch(numbers, 3, null));
    }

    @Test
    public void binarySearch3() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.sort(numbers);
        assertEquals(3, Arrays.binarySearch(numbers, 3, numbers.length, 3, null));
    }

    @Test
    public void equals() throws Exception {
        int[] numbers = new int[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        int[] numbers2 = new int[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        int[] numbers3 = new int[]{5, 5, 1, 3, 1, 9, 2, 4, 7};
        assertEquals(true, Arrays.equals(numbers, numbers2));
        assertEquals(false, Arrays.equals(numbers, numbers3));
    }

    @Test
    public void fill() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.fill(numbers, 1);
        assertEquals(new Integer[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, numbers);
    }

    @Test
    public void fill1() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Arrays.fill(numbers, 0, 2, 1);
        assertEquals(new Integer[]{1, 1, 1, 3, 0, 9, 2, 4, 7}, numbers);
    }

    @Test
    public void copyOf() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        assertEquals(new Integer[]{5, 5, 1}, Arrays.copyOf(numbers, 3));
    }

    @Test
    public void copyOf1() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        assertEquals(new Object[]{5, 5, 1}, Arrays.copyOf(numbers, 3, Object[].class));
    }

    @Test
    public void copyOfRange() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        assertEquals(new Object[]{5, 5, 1}, Arrays.copyOfRange(numbers, 0, 3, Object[].class));
        assertEquals(new Integer[]{5, 5, 1}, Arrays.copyOfRange(numbers, 0, 3));
    }

    @Test
    public void deepEquals() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        Integer[] numbers2 = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        assertEquals(true, Arrays.deepEquals(numbers, numbers2));
    }

    @Test
    public void toStringTest() throws Exception {
        Integer[] numbers = new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7};
        assertEquals(new Integer[]{5, 5, 1, 3, 0, 9, 2, 4, 7}, numbers);
    }

    @Test
    public void deepToString() throws Exception {
        String[] strings = {"aaa", "bbb", "ccc"};
        assertEquals("[aaa, bbb, ccc]", Arrays.deepToString(strings));
    }
}
