package com.getjavajob.training.algo08.kashapovv.lesson10;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by Вадим on 24.12.2016.
 */
public class CollectionsTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    List<Integer> list;

    @Before
    public void setUp() throws Exception {
        list = Arrays.asList(1, 4, 6, 8, 10, 122);
    }

    @Test
    public void sort() throws Exception {
        List<Integer> numbers = Arrays.asList(5, 5, 1, 3, 0, 9, 2, 4, 7);
        Collections.sort(numbers);
        assertEquals(new Integer[]{0, 1, 2, 3, 4, 5, 5, 7, 9}, numbers.toArray());
    }

    @Test
    public void sort1() throws Exception {
        List<Integer> numbers = Arrays.asList(5, 5, 1, 3, 0, 9, 2, 4, 7);
        Collections.sort(numbers, Collections.reverseOrder());
        assertEquals(new Integer[]{9, 7, 5, 5, 4, 3, 2, 1, 0}, numbers.toArray());
    }

    @Test
    public void binarySearch() throws Exception {
        assertEquals(4, Collections.binarySearch(Arrays.asList(1, 4, 6, 8, 10, 122), 10));
    }

    @Test
    public void reverse() throws Exception {
        Collections.reverse(list);
        assertEquals(new Integer[]{122, 10, 8, 6, 4, 1}, list.toArray());
    }

    @Test
    public void shuffle() throws Exception {
        ArrayList<Integer> copy = new ArrayList<>(list);
        Collections.shuffle(list); // [8, 6, 4, 1, 122, 10] , [10, 8, 1, 4, 122, 6] ...
        assertTrue(list.containsAll(copy));
        assertNotEquals(list.toArray(), copy.toArray());
    }

    @Test
    public void swap() throws Exception {
        Collections.swap(list, 3, 0);
        assertEquals(new Integer[]{8, 4, 6, 1, 10, 122}, list.toArray());
    }

    @Test
    public void fill() throws Exception {
        Collections.fill(list, 100);
        assertEquals(new Integer[]{100, 100, 100, 100, 100, 100}, list.toArray());
    }

    @Test
    public void min() throws Exception {
        assertEquals(1, (int) Collections.min(list));
    }

    @Test
    public void max() throws Exception {
        assertEquals(122, (int) Collections.max(list));
    }

    @Test
    public void rotate() throws Exception {
        Collections.rotate(list, 4);
        assertEquals(new Integer[]{6, 8, 10, 122, 1, 4}, list.toArray());
        Collections.rotate(list, 2);
        assertEquals(new Integer[]{1, 4, 6, 8, 10, 122}, list.toArray());
    }

    @Test
    public void replaceAll() throws Exception {
        Collections.replaceAll(list, 4, 20);
        assertEquals(new Integer[]{1, 20, 6, 8, 10, 122}, list.toArray());
    }

    @Test
    public void indexOfSubList() throws Exception {
        assertEquals(3, Collections.indexOfSubList(list, Arrays.asList(8, 10)));
    }

    @Test
    public void lastIndexOfSubList() throws Exception {
        list = Arrays.asList(1, 3, 5, 1, 3, 5, 8, 10);
        assertEquals(4, Collections.lastIndexOfSubList(list, Arrays.asList(3, 5)));
    }

    @Test
    public void checkedCollection() throws Exception {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        Collection col = Collections.checkedCollection(list, Integer.class);
        thrown.expect(ClassCastException.class);
        col.add(2.3);
    }

    @Test
    public void singleton() throws Exception {
        assertEquals("[element]", Collections.singleton("element").toString());
        assertEquals("[element]", Collections.singletonList("element").toString());
        assertEquals("{key=value}", Collections.singletonMap("key", "value").toString());
    }

    @Test
    public void nCopies() throws Exception {
        assertEquals(new Integer[]{5, 5, 5}, Collections.nCopies(3, 5).toArray());
    }

    @Test
    public void enumeration() throws Exception {
        Enumeration<Integer> enumer = Collections.enumeration(Arrays.asList(1, 3, 5, 7, 9, 11));
        assertEquals(1, (int) enumer.nextElement());
        assertEquals(3, (int) enumer.nextElement());
        assertEquals(5, (int) enumer.nextElement());
        assertEquals(7, (int) enumer.nextElement());
        assertEquals(9, (int) enumer.nextElement());
        assertEquals(11, (int) enumer.nextElement());
    }

    @Test
    public void frequency() throws Exception {
        assertEquals(3, Collections.frequency(Arrays.asList(1, 1, 2, 1, 1, 2, 1, 1, 2), 2));
    }

    @Test
    public void disjoint() throws Exception {
        assertTrue(Collections.disjoint(Arrays.asList(1, 3, 5, 7, 9), Arrays.asList(2, 4, 6, 8, 10)));
        assertFalse(Collections.disjoint(Arrays.asList(1, 3, 5, 7, 9), Arrays.asList(2, 4, 5, 8, 10)));
    }

}