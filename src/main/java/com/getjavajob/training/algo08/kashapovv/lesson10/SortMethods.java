package com.getjavajob.training.algo08.kashapovv.lesson10;

import static java.lang.System.arraycopy;

/**
 * Created by Вадим on 22.12.2016.
 */
public class SortMethods {
    static class BubbleSort {
        public static int[] bubbleSort(int[] numbers) {
            for (int i = 0; i < numbers.length - 1; i++) {
                boolean sorted = true;
                for (int j = 0; j < numbers.length - i - 1; j++) {
                    if (numbers[j] > numbers[j + 1]) {
                        int temp = numbers[j + 1];
                        numbers[j + 1] = numbers[j];
                        numbers[j] = temp;
                        sorted = false;
                    }
                }
                if (sorted) {
                    return numbers;
                }
            }
            return numbers;
        }
    }

    static class InsertionSort {
        public static int[] insertionSort(int[] numbers) {
            for (int i = 1; i < numbers.length; i++) {
                int value = numbers[i];
                int j = i;
                while (j > 0 && numbers[j - 1] > value) {
                    numbers[j] = numbers[j - 1];
                    j--;
                }
                numbers[j] = value;
            }
            return numbers;
        }
    }

    static class MergeSort {
        public static int[] mergeSort(int[] numbers) {
            int[] temp = new int[numbers.length];
            return mergeSort(numbers, temp);
        }

        private static int[] mergeSort(int[] numbers, int[] temp) {
            arraycopy(numbers, 0, temp, 0, numbers.length);
            split(numbers, 0, numbers.length, temp);
            arraycopy(temp, 0, numbers, 0, numbers.length);
            return numbers;
        }

        private static void split(int[] temp, int begin, int end, int[] nums) {
            if (end - begin < 2) {
                return;
            }
            int middle = begin - (begin - end) / 2;
            split(nums, begin, middle, temp);
            split(nums, middle, end, temp);
            merge(temp, begin, middle, end, nums);
        }

        private static void merge(int[] nums, int begin, int middle, int end, int[] temp) {
            int i = begin;
            int j = middle;
            for (int k = begin; k < end; k++) {
                if (i < middle && (j >= end || nums[i] <= nums[j])) {
                    temp[k] = nums[i];
                    i++;
                } else {
                    temp[k] = nums[j];
                    j++;
                }
            }
        }
    }

    static class QuickSort {
        public static int[] quickSort(int[] numbers) {
            int end = numbers.length - 1;
            quickSort(numbers, 0, end);
            return numbers;
        }

        private static void quickSort(int[] numbers, int start, int end) {
            if (start >= end) {
                return;
            }
            int i = start;
            int j = end;
            int cur = i - (i - j) / 2;
            while (i < j) {
                while (i < cur && (numbers[i] <= numbers[cur])) {
                    i++;
                }
                while (j > cur && (numbers[cur] <= numbers[j])) {
                    j--;
                }
                if (i < j) {
                    int temp = numbers[i];
                    numbers[i] = numbers[j];
                    numbers[j] = temp;
                    if (i == cur) {
                        cur = j;
                    } else if (j == cur) {
                        cur = i;
                    }
                }
            }
            quickSort(numbers, start, cur);
            quickSort(numbers, cur + 1, end);
        }
    }
}