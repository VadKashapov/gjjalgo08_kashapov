package com.getjavajob.training.algo08.kashapovv.lesson10;

import com.getjavajob.training.algo08.kashapovv.Utils.Assert;
import com.getjavajob.training.algo08.kashapovv.lesson10.SortMethods.BubbleSort;
import com.getjavajob.training.algo08.kashapovv.lesson10.SortMethods.InsertionSort;
import com.getjavajob.training.algo08.kashapovv.lesson10.SortMethods.MergeSort;
import com.getjavajob.training.algo08.kashapovv.lesson10.SortMethods.QuickSort;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by Вадим on 23.12.2016.
 */
public class SortMethodsTest {
    private int[] numbers;

    @Before
    public void setUp() throws Exception {
        numbers = new int[]{5, 5, 1, 3, 0, 9, -1, 4, 7};
    }

    @Test
    public void bubbleSort() throws Exception {
        Assert.assertEquals("bubbleSort", new int[]{0, 1, 2, 3, 4, 5, 5, 7, 9}, BubbleSort.bubbleSort(numbers));
    }

    @Test
    public void insertionSort() throws Exception {
        Assert.assertEquals("insertSort", new int[]{0, 1, 2, 3, 4, 5, 5, 7, 9}, InsertionSort.insertionSort(numbers));
    }

    @Ignore
    @Test(expected = OutOfMemoryError.class)
    public void maxValueSort() throws Exception {
        int[] manyNumbers = new int[MAX_VALUE]; // at least need 32bit*(2^31-1) ~ 8.6 gb of memory heap;
    }

    @Test
    public void mergeSort() throws Exception {
        Assert.assertEquals("mergerSort", new int[]{0, 1, 2, 3, 4, 5, 5, 7, 9}, MergeSort.mergeSort(numbers));
    }

    @Test
    public void quickSort() throws Exception {
        Assert.assertEquals("quickSort", new int[]{0, 1, 2, 3, 4, 5, 5, 7, 9}, QuickSort.quickSort(numbers));
    }

}
